package com.prakritifresh

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.app.Application.ActivityLifecycleCallbacks
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.prakritifresh.utils.Constants

class MyApp : Application(), ActivityLifecycleCallbacks, Constants {

    override fun onCreate() {
        super.onCreate()
        val settings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(false)
                .build()
        FirebaseFirestore.getInstance().firestoreSettings = settings
        //        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/SEGOEUI.TTF");
        val mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .setMinimumFetchIntervalInSeconds(if (BuildConfig.DEBUG) 0 else 3600)
                .build()
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings)
                .addOnCompleteListener { task: Task<Void?> -> Log.i(Constants.TAG, "onCreate: " + task.isSuccessful) }
        mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        mFirebaseRemoteConfig.fetchAndActivate()
                 .addOnCompleteListener { task: Task<Boolean?> ->
                     if (task.isSuccessful) {
                         val updated = task.result!!
                         Log.d(Constants.TAG, "Config params updated: $updated")
                     } else {
                         Toast.makeText(applicationContext, "Fetch failed",
                                 Toast.LENGTH_SHORT).show()
                     }
                 }

        Thread { Glide.get(applicationContext).clearDiskCache() }.start()
        FirebaseAuth.getInstance().currentUser?.let {
            FirebaseCrashlytics.getInstance().setUserId(it.uid);
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle) {
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onActivityStarted(activity: Activity) {}
    override fun onActivityResumed(activity: Activity) {}
    override fun onActivityPaused(activity: Activity) {}
    override fun onActivityStopped(activity: Activity) {}
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
    override fun onActivityDestroyed(activity: Activity) {}

    companion object {
        val db: FirebaseFirestore
            get() = FirebaseFirestore.getInstance()
    }
}