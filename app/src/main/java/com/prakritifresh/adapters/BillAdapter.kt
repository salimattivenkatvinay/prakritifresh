package com.prakritifresh.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.mrittica.OrderItem
import com.mrittica.utils.roundToCurrency
import com.mrittica.utils.toTitleCase
import com.prakritifresh.R
import java.util.*
import kotlin.math.roundToInt

class BillAdapter(c: Context, private val data: ArrayList<OrderItem>) : BaseAdapter() {
    private val inflater: LayoutInflater = c.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    override fun getCount() = data.size
    override fun getItem(position: Int) = position
    override fun getItemId(position: Int) = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            view = inflater.inflate(R.layout.listview_bill_layout, null)!!
        }
        val dat: OrderItem = data[position]
        val billnm = view.findViewById<TextView>(R.id.billnm)
        val billam = view.findViewById<TextView>(R.id.billam)
        val billpr = view.findViewById<TextView>(R.id.billpr)
        val billqt = view.findViewById<TextView>(R.id.billqt)
        billnm.text = (position + 1).toString() + "." +
                if (dat.subcat.equals("main", ignoreCase = true)) dat.name.toTitleCase() else dat.subcat.toTitleCase()
        billpr.text = "" + dat.cost!!.toFloat().roundToInt()
        billqt.text = String.format("%.2f", dat.number)
        billam.text = "₹" + (dat.number * dat.cost!!.toFloat().roundToInt()).roundToCurrency()
        return view
    }
}