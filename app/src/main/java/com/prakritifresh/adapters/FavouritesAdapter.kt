package com.prakritifresh.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.like.LikeButton
import com.like.OnLikeListener
import com.mrittica.ShopItem
import com.prakritifresh.GlideApp
import com.prakritifresh.R
import com.prakritifresh.activities.ItemDetailsActivity
import com.prakritifresh.listners.UpdateListener
import com.prakritifresh.utils.SharedPrefsUtil
import java.util.*
import java.util.concurrent.atomic.AtomicReference

class FavouritesAdapter(private val context: Context, private val cartListner: UpdateListener?) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var likeList: ArrayList<String>

    override fun getCount(): Int {
        return likeList.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var f5v = convertView

        if (f5v == null) {
            f5v = inflater.inflate(R.layout.gridview_layout_variety, null)!!
        }
        val imageView = f5v.findViewById<ImageView>(R.id.imgrid1)
        val imageView1 = f5v.findViewById<ImageView>(R.id.imout1)
        val textView = f5v.findViewById<TextView>(R.id.vegname1)
        val textView1 = f5v.findViewById<TextView>(R.id.prc1)
        val cart = f5v.findViewById<Button>(R.id.add_to_cart1)
        val likeButton: LikeButton = f5v.findViewById(R.id.like_button)
        val prod = AtomicReference<ShopItem?>()

        FirebaseFirestore.getInstance()
                .collection("shopitems")
                .document(likeList[position])
                .get()
                .addOnSuccessListener { documentSnapshot1: DocumentSnapshot ->
                    if (documentSnapshot1.exists()) {
                        prod.set(documentSnapshot1.toObject(ShopItem::class.java))
                        prod.get()!!.id = documentSnapshot1.id
                        textView.text = prod.get()!!.name + ":" + prod.get()!!.subcat
                        textView1.text = "₹" + Math.round(prod.get()!!.cost.toFloat()) + "/ " + prod.get()!!.unittype.trim { it <= ' ' }.trimEnd { it == 's' }
                        val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/" + prod.get()!!.itemID + ".jpg")
                        GlideApp.with(context)
                                .load(storageReference)
                                .error(R.mipmap.prakritifresh_logo)
                                .into(imageView)
                        if (prod.get()!!.available == "no") {
                            imageView1.visibility = View.VISIBLE
                            cart.visibility = View.INVISIBLE
                        }
                        if (likeList.contains(prod.get()!!.id)) {
                            likeButton.setLiked(true)
                        } else {
                            likeButton.setLiked(false)
                        }
                        if (SharedPrefsUtil.getInstance(context).isInCart(prod.get()?.id)) {
                            cart.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                            cart.text = "Added to cart"
                            cart.background = null
                            cart.setTextColor(context.resources.getColor(R.color.orange))
                            cart.isClickable = false
                            cart.isEnabled = false
                        } else {
                            cart.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_input_add, 0, 0, 0)
                            cart.text = "Add to cart"
                            cart.background = context.resources.getDrawable(R.drawable.add_to_cart_bg)
                            cart.setTextColor(context.resources.getColor(R.color.white))
                            cart.isClickable = true
                            cart.isEnabled = true
                        }
                        likeButton.setOnLikeListener(object : OnLikeListener {
                            override fun liked(likeButton1: LikeButton) {
                                SharedPrefsUtil.getInstance(context).addToFavourites(prod.get()!!.id)
                                likeList = SharedPrefsUtil.getInstance(context).likesList
                                notifyDataSetChanged()
                            }

                            override fun unLiked(likeButton1: LikeButton) {
                                SharedPrefsUtil.getInstance(context).removeFromFavourites(prod.get()!!.id)
                                likeList = SharedPrefsUtil.getInstance(context).likesList
                                notifyDataSetChanged()
                            }
                        })
                        cart.setOnClickListener { v: View? ->
                            SharedPrefsUtil.getInstance(context).addToCart(prod.get()?.id, prod.get()?.unitqty?.toFloat())
                            cart.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                            cart.text = "Added to cart"
                            cart.background = null
                            cart.setTextColor(context.resources.getColor(R.color.orange))
                            cart.isClickable = false

                            cartListner?.cartUpdate()
                        }

                        imageView?.setOnClickListener {
                            context.startActivity(Intent(context, ItemDetailsActivity::class.java).putExtra("item", prod.get()))
                            Animatoo.animateSlideUp(context)
                        }
                    } else {
                        SharedPrefsUtil.getInstance(context).removeFromFavourites(likeList[position])
                        likeList = SharedPrefsUtil.getInstance(context).likesList
                        notifyDataSetChanged()
                    }
                }
        f5v.scaleX = 0.9f
        f5v.scaleY = 0.9f
        return f5v
    }

    init {
        likeList = SharedPrefsUtil.getInstance(context).likesList
    }
}