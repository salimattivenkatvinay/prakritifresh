package com.prakritifresh.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.firebase.storage.FirebaseStorage
import com.prakritifresh.GlideApp
import com.prakritifresh.R
import com.prakritifresh.activities.GridFragmentVarietyActivity
import com.mrittica.ShopItem
import com.mrittica.utils.toTitleCase

class GridAdapter(private val context: Context?, private val data: MutableList<ShopItem>) : BaseAdapter() {
    private var inflater: LayoutInflater? = null
    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var f5v = convertView
        if (convertView == null) {
            f5v = inflater!!.inflate(R.layout.gridview_layout, null)
        }
        val imageView = f5v!!.findViewById<ImageView>(R.id.imgrid1)
        val imageView1 = f5v.findViewById<ImageView>(R.id.imout1)
        val textView = f5v.findViewById<TextView>(R.id.vegname1)
        val prod: ShopItem = data[position]
        if (prod.available == "no" || prod.unitqty.toFloat() > prod.available_quantity) {
            imageView1.visibility = View.VISIBLE
        } else {
            imageView1.visibility = View.INVISIBLE
        }

        //data.size();
        textView.text = prod.name.toTitleCase()
        val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/" + prod.itemID + ".jpg")
        GlideApp.with(context!!)
                .load(storageReference)
                .thumbnail(0.1f)
                .error(R.mipmap.prakritifresh_logo)
                .into(imageView)
        f5v.setOnClickListener { v: View? ->
            context?.startActivity(Intent(context, GridFragmentVarietyActivity::class.java)
                    .putExtra("type", prod.category)
                    .putExtra("name", prod.name))
            Animatoo.animateSlideLeft(context); //fire the slide left animation
        }
        /*f5v.setScaleX(0.9f);
        f5v.setScaleY(0.9f);*/return f5v
    }

    init {
        if (context != null) inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}