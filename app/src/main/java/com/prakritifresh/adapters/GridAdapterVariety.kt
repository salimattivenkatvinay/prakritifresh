package com.prakritifresh.adapters

import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.firebase.storage.FirebaseStorage
import com.like.LikeButton
import com.like.OnLikeListener
import com.mrittica.ShopItem
import com.mrittica.utils.toTitleCase
import com.prakritifresh.GlideApp
import com.prakritifresh.R
import com.prakritifresh.activities.ItemDetailsActivity
import com.prakritifresh.listners.UpdateListener
import com.prakritifresh.utils.SharedPrefsUtil
import java.util.*


class GridAdapterVariety(private val context: Context, private var data: ArrayList<ShopItem>) : BaseAdapter() {
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val likeSet: Set<String> = SharedPrefsUtil.getInstance(context).likesSet
    private var updateListener: UpdateListener? = null

    constructor(a: Context, d: ArrayList<ShopItem>, updateListener: UpdateListener?) : this(a, d) {
        this.updateListener = updateListener
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, fv: View?, parent: ViewGroup): View {
        var convertView = fv

        if (convertView == null)
            convertView = inflater.inflate(R.layout.gridview_layout_variety, null)!!

        val imageView = convertView.findViewById<ImageView>(R.id.imgrid1)
        val imageView1 = convertView.findViewById<ImageView>(R.id.imout1)
        val vegname1 = convertView.findViewById<TextView>(R.id.vegname1)
        val textView1 = convertView.findViewById<TextView>(R.id.prc1)
        val cart = convertView.findViewById<Button>(R.id.add_to_cart1)
        val likeButton: LikeButton = convertView.findViewById(R.id.like_button)
        val prod: ShopItem = data[position]
        //data.size();
        vegname1.text = if (prod.subcat == "main") Html.fromHtml(prod.name.toTitleCase() + "<br><small><i>" + prod.name.toTitleCase() + "</i></small>")
        else Html.fromHtml(prod.subcat.toTitleCase() + "<br><small><i>" + prod.name.toTitleCase() + "</i></small>")

        textView1.text = "₹" + Math.round(prod.cost.toFloat()) + "/ " + prod.unittype.trim().trimEnd { it == 's' }
        val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/" + prod.itemID + ".jpg")
        GlideApp.with(context)
                .load(storageReference)
                .thumbnail(0.1f)
                .error(R.mipmap.prakritifresh_logo)
                .into(imageView)
        //Log.i("vinay", prod.toString())
        if (prod.available == "no" || prod.unitqty.toFloat() > prod.available_quantity) {
            imageView1.visibility = View.VISIBLE
            cart.visibility = View.INVISIBLE
        } else {
            imageView1.visibility = View.INVISIBLE
            cart.visibility = View.VISIBLE
        }
        likeButton.isLiked = likeSet.contains(prod.id)

        val scale: Float = context.resources.displayMetrics.density
        val dpAsPixels = (8 * scale + 0.5f).toInt()

        if (SharedPrefsUtil.getInstance(context).isInCart(prod.id)) {
            cart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_minus_yellow, 0, 0, 0)
            cart.text = "Remove"
            cart.background = context.resources.getDrawable(R.drawable.added_to_cart_bg)
//            cart.setTextColor(context.resources.getColor(R.color.orange))
            cart.setPadding(dpAsPixels, 0, 0, 0)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                cart.textAlignment = View.TEXT_ALIGNMENT_CENTER
            }
        } else {
            cart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_plus_yellow, 0, 0, 0)
            cart.text = "Add to cart"
            cart.background = context.resources.getDrawable(R.drawable.add_to_cart_bg)
//            cart.setTextColor(context.resources.getColor(R.color.white))
            cart.setPadding(dpAsPixels, 0, 0, 0)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                cart.textAlignment = View.TEXT_ALIGNMENT_CENTER
            }
        }

        likeButton.setOnLikeListener(object : OnLikeListener {
            override fun liked(likeButton: LikeButton) {
                SharedPrefsUtil.getInstance(context).addToFavourites(prod.id)
            }

            override fun unLiked(likeButton: LikeButton) {
                SharedPrefsUtil.getInstance(context).removeFromFavourites(prod.id)
            }
        })
        cart.setOnClickListener {
            if (!SharedPrefsUtil.getInstance(context).isInCart(prod.id)) {
                SharedPrefsUtil.getInstance(context).addToCart(prod.id, prod.unitqty.toFloat())
                cart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_minus_yellow, 0, 0, 0)
                cart.text = "Remove"
                cart.background = context.resources.getDrawable(R.drawable.added_to_cart_bg)
//                cart.setTextColor(context.resources.getColor(R.color.orange))
                cart.setPadding(dpAsPixels, 0, dpAsPixels, 0)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    cart.textAlignment = View.TEXT_ALIGNMENT_CENTER
                }
            } else {
                SharedPrefsUtil.getInstance(context).removeFromCart(prod.id)
                cart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_plus_yellow, 0, 0, 0)
                cart.text = "Add to cart"
                cart.background = context.resources.getDrawable(R.drawable.add_to_cart_bg)
//                cart.setTextColor(context.resources.getColor(R.color.white))
                cart.setPadding(dpAsPixels, 0, dpAsPixels, 0)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    cart.textAlignment = View.TEXT_ALIGNMENT_CENTER
                }
            }
            if (updateListener != null) {
                updateListener?.cartUpdate()
            }
        }

        imageView?.setOnClickListener {
            context.startActivity(Intent(context, ItemDetailsActivity::class.java).putExtra("item", prod))
            Animatoo.animateSlideUp(context)
        }
        convertView.scaleX = 0.9f
        convertView.scaleY = 0.9f
        return convertView
    }

    fun setFilter(FilteredDataList: ArrayList<ShopItem>) {
        data = FilteredDataList
        notifyDataSetChanged()
    }
}