package com.prakritifresh.pojos

import com.mrittica.constants.OrderStatus
import java.util.*
import kotlin.collections.ArrayList

class SubscriptionF {
    var id: String? = null
    var startDate: Date? = null
    var endDate: Date? = null
    var paidAmount: Float = 0F
    var userId: String? = null
    var shopId: String? = null
    var address: String? = null
    var phno: String? = null
    var lat: Double? = null
    var lon: Double? = null
    var pincode: String? = null
    var landmark: String? = null
    var status = OrderStatus.PENDING
    var itemsSubscribed: MutableList<String> = ArrayList()
}
