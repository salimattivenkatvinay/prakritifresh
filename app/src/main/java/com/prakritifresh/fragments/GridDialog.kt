package com.prakritifresh.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.res.ResourcesCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.mrittica.ShopItem
import com.prakritifresh.R
import com.prakritifresh.adapters.GridAdapter
import com.prakritifresh.utils.SharedPrefsUtil
import com.prakritifresh.viewModels.ShopItemsViewModel
import java.util.*
import kotlin.math.ceil

class GridDialog : AppCompatDialogFragment(), ViewPager.OnPageChangeListener {
    private var db: FirebaseFirestore? = null

    private lateinit var intro_images: ViewPager
    private var pager_indicator: LinearLayout? = null
    private var loading: ProgressBar? = null
    private var tvar: View? = null
    private var tv_catch_phrase: View? = null
    private var dotsCount = 0
    private lateinit var dots: Array<ImageView?>
    private var mAdapter: ViewPagerAdapter? = null
    private var currentPage = 0

    private val catComparator = Comparator<ShopItem> { a, b -> a.name!!.compareTo(b.name!!) }
    private var aList: SortedSet<ShopItem> = TreeSet(catComparator)
    private var bList: SortedSet<ShopItem> = TreeSet(catComparator)
    val sList = ArrayList<ShopItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setStyle(STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog_NoActionBar)
        setStyle(STYLE_NORMAL, R.style.DialogSlideAnim)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.primary_grid_layout, null)

        view.findViewById<ImageView>(R.id.ivClose)
                .setOnClickListener {
                    dismiss()
                }
        intro_images = view.findViewById(R.id.pager_introduction)
        pager_indicator = view.findViewById(R.id.viewPagerCountDots)
        loading = view.findViewById(R.id.loading)
        tvar = view.findViewById(R.id.tvar)
        tv_catch_phrase = view.findViewById(R.id.tv_catch_phrase)

        getJSON(arguments!!.getString("type"))
/*        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.setWindowAnimations(R.style.DialogAnimation)*/

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.argb(10, 10, 10, 10)))

        val window: WindowManager = context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        val display: Display = window.defaultDisplay
        val scale = context!!.resources.displayMetrics.density
        val pixels = (535 * scale + 0.5f)
        dialog!!.window!!.setLayout(display.width, display.height)
        return view
    }

    private fun getJSON(type: String?) {
/*        db = FirebaseFirestore.getInstance()
        db!!.collection("pincodes")
                .document(SharedPrefsUtil.getInstance(context).pincode)
                .get()
                .addOnSuccessListener { documentSnapshot: DocumentSnapshot ->
                    if (!documentSnapshot.exists()) {
                        loading?.visibility = View.GONE
                        tvar.visibility = View.VISIBLE
                        return@addOnSuccessListener
                    }
                    val temp = documentSnapshot.data as Map<String, *>?
                    if (temp != null) {
                        db!!.collection("shopitems")
                                .whereIn("pincode", temp.keys.toMutableList())
                                .whereEqualTo("category", type)
                                .get()
                                .addOnSuccessListener { queryDocumentSnapshots: QuerySnapshot ->
                                    if (!queryDocumentSnapshots.isEmpty) {
                                        for (documentSnapshot1 in queryDocumentSnapshots) {
                                            val shopItem = documentSnapshot1.toObject(ShopItem::class.java)

                                            if (aList.contains(shopItem) && !bList.contains(shopItem))
                                                continue
                                            else if (!aList.contains(shopItem) && !bList.contains(shopItem)) {
                                                shopItem.id = documentSnapshot1.id
                                                if (shopItem.available == "no" || shopItem.unitqty.toFloat() > shopItem.available_quantity) {
                                                    bList.add(shopItem)
                                                } else {
                                                    aList.add(shopItem)
                                                }
                                            } else {
                                                val l = bList.find { it.name == shopItem.name }
                                                if (l!!.available == "no" || l.unitqty.toFloat() > l.available_quantity) {
                                                    if (shopItem.available == "yes" && shopItem.unitqty.toFloat() <= shopItem.available_quantity) {
                                                        bList.remove(l)
                                                        aList.add(shopItem)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    loading?.visibility = View.GONE
                                    showJSON()

                                }
                                .addOnFailureListener { e: Exception ->
                                    Toast.makeText(context, e.localizedMessage, Toast.LENGTH_SHORT).show()
                                    loading?.visibility = View.GONE
                                }
                    } else {
//                        loading.visibility = View.GONE;
                    }
                }
                .addOnFailureListener { e: Exception ->
                    Toast.makeText(context, e.localizedMessage, Toast.LENGTH_SHORT).show()
                    loading?.visibility = View.GONE
                }*/

        ShopItemsViewModel.getFilterdVarietyShopItems(SharedPrefsUtil.getInstance(context).pincode, true, type) {
            it.forEach { shopItem ->
                if (aList.contains(shopItem) && !bList.contains(shopItem))

                else if (!aList.contains(shopItem) && !bList.contains(shopItem)) {
                    if (shopItem.available == "no" || shopItem.unitqty.toFloat() > shopItem.available_quantity) {
                        bList.add(shopItem)
                    } else {
                        aList.add(shopItem)
                    }
                } else {
                    val l = bList.find { it.name == shopItem.name }
                    if (l!!.available == "no" || l.unitqty.toFloat() > l.available_quantity) {
                        if (shopItem.available == "yes" && shopItem.unitqty.toFloat() <= shopItem.available_quantity) {
                            bList.remove(l)
                            aList.add(shopItem)
                        }
                    }
                }
            }
            loading?.visibility = View.GONE
            showJSON()
        }
    }

    private fun showJSON() {
        sList.addAll(aList)
        sList.addAll(bList)

        if (sList.size > 0 && context != null) {
            mAdapter = ViewPagerAdapter(context!!, sList)
            intro_images.adapter = mAdapter
            intro_images.currentItem = 0
            intro_images.addOnPageChangeListener(this)
            setUiPageViewController()
        } else {
            tvar?.visibility = View.VISIBLE
            tv_catch_phrase?.visibility = View.GONE
        }
    }


    private fun setUiPageViewController() {
        dotsCount = mAdapter!!.count
        dots = arrayOfNulls(dotsCount)
        for (i in 0 until dotsCount) {
            dots[i] = ImageView(context)
            dots[i]!!.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.nonselecteditem_dot, null))
            val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(4, 0, 4, 0)
            pager_indicator!!.addView(dots[i], params)
        }
        dots[0]!!.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.selecteditem_dot, null))
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    override fun onPageSelected(position: Int) {
        if (!isVisible) {
            return
        }
        currentPage = position
        for (i in 0 until dotsCount) {
            dots[i]!!.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.nonselecteditem_dot, null))
        }
        dots[position]!!.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.selecteditem_dot, null))
    }

    override fun onPageScrollStateChanged(state: Int) {}


    class ViewPagerAdapter(private val mContext: Context, private val sList: ArrayList<ShopItem>) : PagerAdapter() {
        override fun getCount(): Int = ceil(sList.size / 9.0).toInt()

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as GridView
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = LayoutInflater.from(mContext).inflate(R.layout.grid_item, container, false)
            val gridView = itemView.findViewById<GridView>(R.id.gridview1)
            val adapter = GridAdapter(mContext, sList.subList(9 * position, if (9 * (position + 1) > sList.size) sList.size else (9 * (position + 1))))
            gridView.adapter = adapter
            container.addView(itemView)
            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as GridView)
        }
    }
}