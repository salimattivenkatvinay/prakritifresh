package com.prakritifresh.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.faltenreich.skeletonlayout.Skeleton
import com.faltenreich.skeletonlayout.SkeletonLayout
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.mrittica.Order
import com.mrittica.ShopItem
import com.mrittica.constants.OrderStatus
import com.prakritifresh.MyApp
import com.prakritifresh.R
import com.prakritifresh.activities.HomeActivity
import com.prakritifresh.activities.OrderDetailsActivity
import com.prakritifresh.activities.SearchActivity
import com.prakritifresh.adapters.GridAdapterVariety
import com.prakritifresh.adapters.ViewPagerAdapter
import com.prakritifresh.customViews.HorizontalListView
import com.prakritifresh.utils.Constants
import com.prakritifresh.utils.SharedPrefsUtil
import com.prakritifresh.viewModels.ShopItemsViewModel
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.util.*

class HomeFragment : Fragment(), View.OnClickListener, ViewPager.OnPageChangeListener, Constants {

    private lateinit var intro_images: ViewPager
    private var pager_indicator: LinearLayout? = null
    private var dotsCount = 0
    private lateinit var dots: Array<ImageView?>
    private var mAdapter: ViewPagerAdapter? = null
    private var currentPage = 0

    private var lv_pop: HorizontalListView? = null
    private var lv_popr: HorizontalListView? = null

    private var tv_Replace: TextView? = null
    private var tv_lastOrder: TextView? = null

    val popList = ArrayList<ShopItem>()
    val newList = ArrayList<ShopItem>()

    private var skeleton: Skeleton? = null

    private lateinit var viewOfLayout: View


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewOfLayout = inflater.inflate(R.layout.fragment_home, null)
        lv_pop = viewOfLayout.lv_pop
        lv_popr = viewOfLayout.lv_popr
        tv_Replace = viewOfLayout.tv_Replace
        tv_lastOrder = viewOfLayout.tv_lastOrder
        viewOfLayout.ic_search.setOnClickListener(this)
        viewOfLayout.ivVegetables.setOnClickListener(this)
        viewOfLayout.ivfruits.setOnClickListener(this)
        viewOfLayout.ivGroceries.setOnClickListener(this)

        intro_images = viewOfLayout.findViewById(R.id.pager_introduction)
        pager_indicator = viewOfLayout.findViewById(R.id.viewPagerCountDots)

        mAdapter = ViewPagerAdapter(activity)
        intro_images.adapter = mAdapter
        intro_images.currentItem = 0
        intro_images.addOnPageChangeListener(this)
        setUiPageViewController()
        val handler = Handler()
        val timer = Timer()
        val DELAY_MS: Long = 3000
        val PERIOD_MS: Long = 3000
        timer.schedule(object : TimerTask() {
            override fun run() {
                handler.post {
                    currentPage++
                    intro_images.setCurrentItem(
                        currentPage % intro_images.adapter?.count!! - 1,
                        true
                    )
                }
            }
        }, DELAY_MS, PERIOD_MS)

        skeleton = viewOfLayout.findViewById<SkeletonLayout>(R.id.skeletonLayout);
        skeleton?.showSkeleton()
        getPopularItems("popular")
        getPopularItems("rice")
        return viewOfLayout
    }

    override fun onResume() {
        super.onResume()
        lv_pop?.setAdapter(GridAdapterVariety(context!!, popList, activity as HomeActivity))
        lv_popr?.setAdapter(GridAdapterVariety(context!!, newList, activity as HomeActivity))

        if (null != FirebaseAuth.getInstance().currentUser) {
            FirebaseFirestore.getInstance().collection("replaceRequest")
                .orderBy("shop_id")
                .whereEqualTo("user_id", FirebaseAuth.getInstance().currentUser?.uid)
                .whereEqualTo("status", "PENDING")
                .orderBy("reqTime", Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnSuccessListener { querySnapshot ->
                    querySnapshot.forEach {
                        val n = it.data as HashMap<String, String>;
                        n["id"] = it.id
                        tv_Replace?.visibility = View.VISIBLE
                        tv_Replace?.isSelected = true
                    }

                }
            FirebaseFirestore.getInstance().collection("orders")
                .whereEqualTo("user_id", FirebaseAuth.getInstance().currentUser!!.uid)
//                    .whereEqualTo("status", OrderStatus.DELIVERED)
                .orderBy("orderedTime", Query.Direction.DESCENDING)
                .limit(1L)
                .get()
                .addOnCompleteListener { task: Task<QuerySnapshot> ->
                    if (task.isSuccessful)
                        for (documentSnapshot in task.result!!) {
                            val m = documentSnapshot.toObject(Order::class.java)
                            m.order_id = documentSnapshot.id
                            if (!m.status.equals(OrderStatus.DELIVERED)) {

                                activity?.runOnUiThread {
                                    tv_lastOrder?.visibility = View.VISIBLE
                                    tv_lastOrder?.isSelected = true
                                    tv_lastOrder?.setText("You last order is : " + m.status)

                                    tv_lastOrder?.setOnClickListener {
                                        startActivity(
                                            Intent(context, OrderDetailsActivity::class.java)
                                                .putExtra("order", m)
                                        )
                                    }
                                }
                            }
                        } else Log.i(Constants.TAG, task.exception!!.message)
                }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.ic_search -> {
                startActivity(Intent(context, SearchActivity::class.java))
                return
            }
            R.id.ivVegetables, R.id.ivfruits, R.id.ivGroceries -> {
                val dialogFragment = GridDialog()
                val bundle = Bundle()
                when (v.id) {
                    R.id.ivVegetables -> bundle.putString("type", Constants.VEGETABLES)
                    R.id.ivfruits -> bundle.putString("type", Constants.FRUITS)
                    R.id.ivGroceries -> bundle.putString("type", Constants.STAPLES)
                }
                dialogFragment.arguments = bundle
                val ft = fragmentManager!!.beginTransaction()
                val prev = fragmentManager!!.findFragmentByTag("dialog")
                if (prev != null) {
                    ft.remove(prev)
                }
                ft.addToBackStack(null)
                dialogFragment.show(ft, "dialog")
                return
            }
            else -> {
            }
        }
    }

    private fun setUiPageViewController() {
        dotsCount = mAdapter!!.count
        dots = arrayOfNulls(dotsCount)
        for (i in 0 until dotsCount) {
            dots[i] = ImageView(context)
            dots[i]!!.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.nonselecteditem_dot,
                    null
                )
            )
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(4, 0, 4, 0)
            pager_indicator!!.addView(dots[i], params)
        }
        dots[0]!!.setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.selecteditem_dot,
                null
            )
        )
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    override fun onPageSelected(position: Int) {
        if (!isVisible) {
            return
        }
        currentPage = position
        for (i in 0 until dotsCount) {
            dots[i]!!.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.nonselecteditem_dot,
                    null
                )
            )
        }
        dots[position]!!.setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.selecteditem_dot,
                null
            )
        )
    }

    override fun onPageScrollStateChanged(state: Int) {}

    private fun getPopularItems(key: String) {
        MyApp.db.collection("popularItems")
            .document(SharedPrefsUtil.getInstance(context).pincode)
            .collection(key)
            .get()
            .addOnSuccessListener { queryDocumentSnapshots ->
                if (!queryDocumentSnapshots.isEmpty || true) {
                    val aList = ArrayList<ShopItem>()
                    val documents = queryDocumentSnapshots.documents;

                    ShopItemsViewModel.getFilteredShopItems(
                        SharedPrefsUtil.getInstance(context).pincode,
                        false,
                        documents.map { it.id }) {
                        if (context == null) {
                            //Toast.makeText(context, "", Toast.LENGTH_SHORT).show()
                        } else {
                            if (key == "popular") {
                                (skeleton as SkeletonLayout).visibility = View.GONE
                                popList.clear()
                                if (it.isNotEmpty())
                                    popList.addAll(it)
                                else
                                    popList.addAll(
                                        ShopItemsViewModel.getCachedAllShopItems(false).shuffled()
                                            .filterIndexed { index, shopItem -> index < 11 })
                                lv_pop?.setAdapter(
                                    GridAdapterVariety(
                                        context!!,
                                        popList,
                                        activity as HomeActivity
                                    )
                                )
                            } else {
                                newList.clear()
                                if (it.isNotEmpty())
                                    newList.addAll(it)
                                else
                                    newList.addAll(ShopItemsViewModel.getCachedAllShopItems(false)
                                        .filter { it.category == Constants.STAPLES }.shuffled()
                                        .filterIndexed { index, shopItem -> index < 11 })
                                lv_popr?.setAdapter(
                                    GridAdapterVariety(
                                        context!!,
                                        newList,
                                        activity as HomeActivity
                                    )
                                )
                            }
                        }
                    }
                }
            }
    }
}
