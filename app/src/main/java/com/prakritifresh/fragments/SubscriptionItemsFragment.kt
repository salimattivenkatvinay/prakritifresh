package com.prakritifresh.fragments

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import com.mrittica.OrderItem
import com.mrittica.ShopItem
import com.mrittica.constants.OrderStatus
import com.mrittica.constants.OrderTypes
import com.prakritifresh.GlideApp
import com.prakritifresh.MyApp
import com.prakritifresh.R
import com.prakritifresh.activities.BillActivity
import com.prakritifresh.activities.LoginActivity
import com.prakritifresh.activities.SubscriptionDateSelectActivity
import com.prakritifresh.utils.Constants.CART_LIST
import com.prakritifresh.utils.Constants.orderType
import com.prakritifresh.utils.SharedPrefsUtil
import com.prakritifresh.viewModels.SubscriptionViewModel
import kotlinx.android.synthetic.main.fragment_subscripton_items.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt
import kotlin.time.DurationUnit
import kotlin.time.ExperimentalTime
import kotlin.time.milliseconds

class SubscriptionItemsFragment : Fragment() {

    private val data = ArrayList<ShopItem>()
    private lateinit var gv_subscriptions: GridView
    private lateinit var pb_subscriptions: ProgressBar
    private lateinit var ll_subscription_info: LinearLayout

    @ExperimentalTime
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_subscripton_items, container, false)
        view.btn_subscribe.setOnClickListener {
            if (!FirebaseRemoteConfig.getInstance().getBoolean("is_sub_active")) {
                context?.let { it1 ->
                    AlertDialog.Builder(it1)
                            .setTitle("All SubscriptionF slots full for this month")
                            .setNegativeButton("ok") { d: DialogInterface, _: Int -> d.dismiss() }
                            .create()
                            .show()
                }
                return@setOnClickListener
            }
            if (FirebaseAuth.getInstance().currentUser == null) {
                context?.let { it1 ->
                    AlertDialog.Builder(it1)
                            .setTitle("Login to Subscribe")
                            .setPositiveButton("Login") { _: DialogInterface?, _: Int -> startActivity(Intent(it1, LoginActivity::class.java)) }
                            .setNegativeButton("cancel") { d: DialogInterface, _: Int -> d.dismiss() }
                            .create()
                            .show()
                }
                return@setOnClickListener
            }
            val shopItem = OrderItem()
            shopItem.number = 1F
            shopItem.name = "Plus"
            shopItem.subcat = "Plus membership"
            shopItem.cost = "99"
            shopItem.unittype = ""
            val temp = FirebaseRemoteConfig.getInstance().getString("shop_ids")
            shopItem.shop = Gson().fromJson(temp, Map::class.java)[SharedPrefsUtil.getInstance(requireContext()).pincode]?.toString()

            startActivity(Intent(activity, BillActivity::class.java)
                    .putExtra(CART_LIST, arrayListOf(shopItem))
                    .putExtra(orderType, OrderTypes.PLUS.name))
        }

        pb_subscriptions = view.pb_subscriptions
        gv_subscriptions = view.gv_subscriptions
        ll_subscription_info = view.ll_subscription_info

/*        if (SharedPrefsUtil.getInstance(context).isPlusUser) {
            ll_subscription_info.visibility = View.GONE
            gv_subscriptions.visibility = View.VISIBLE
            getData()
        }*/

        SubscriptionViewModel.isPlusUser {
            when (it) {
                OrderStatus.DELIVERED -> {
                    if (Date() < Date(2020 - 1900, 10, 19)) {
                        view.tv_sp.visibility = View.VISIBLE

                        val days = (Date().time - Date(2020 - 1900, 10, 19).time).milliseconds.toInt(DurationUnit.DAYS)
                        view.tv_sp.text = "Subscriptions will start in ${days} days "
                    } else {
                        gv_subscriptions.visibility = View.VISIBLE
                        getData()
                    }
                }
                OrderStatus.PENDING -> {
                    view.tv_sp.visibility = View.VISIBLE
                }
                else -> {
                    if (Date() < Date(2020 - 1900, 10, 19)) {
                        view.tv_register.visibility = View.VISIBLE
                    }
                    ll_subscription_info.visibility = View.VISIBLE
                }
            }
        }
        return view
    }

    private fun getData() {
        pb_subscriptions.visibility = View.VISIBLE
    /*    MyApp.db.collection("pincodes")
                .document(SharedPrefsUtil.getInstance(context).pincode)
                .get()
                .addOnSuccessListener { documentSnapshot ->
                    val temp = documentSnapshot.data
                    if (temp != null)*/
                        MyApp.db.collection("shopitems")
                                .whereIn("pincode", /*temp.keys.toMutableList()*/listOf(SharedPrefsUtil.getInstance(context).pincode))
                                .whereEqualTo("delivery_type_selector", "0")
                                .get()
                                .addOnSuccessListener { queryDocumentSnapshots ->
                                    if (!queryDocumentSnapshots.isEmpty) {
                                        data.clear()
                                        for (documentSnapshot1 in queryDocumentSnapshots) {
                                            val name = documentSnapshot1.toObject(ShopItem::class.java)
                                            name.id = documentSnapshot1.id
                                            if (name.available == "no")
                                                data.add(name)
                                            else
                                                data.add(0, name)
                                        }
                                    }
                                    pb_subscriptions.visibility = View.GONE
                                    showJSON()

                                }
                                .addOnFailureListener { e ->
                                    pb_subscriptions.visibility = View.GONE
                                }
//                }
//                .addOnFailureListener { e -> pb_subscriptions.visibility = View.GONE; }
    }

    private fun showJSON() {

        gv_subscriptions.adapter = object : BaseAdapter() {

            private var inflater: LayoutInflater? = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            override fun getView(position: Int, convertView: View?, p2: ViewGroup?): View? {
                var f5v = convertView
                if (f5v == null) {
                    f5v = inflater?.inflate(R.layout.row_subscription_items, null)
                }

                val prod: ShopItem = data[position]
                val tvSubscriptionItemName = f5v?.findViewById<TextView>(R.id.tv_subscription_item_name)
                val tvSubscriptionItemPrice = f5v?.findViewById<TextView>(R.id.tv_subscription_item_price)
                val ivSubscriptionItem = f5v?.findViewById<ImageView>(R.id.iv_subscription_item)
                val ivSubscriptionOutofstock = f5v?.findViewById<ImageView>(R.id.iv_subscription_outofstock)

                tvSubscriptionItemName!!.text = if (prod.subcat == "main") prod.name else prod.subcat
                tvSubscriptionItemPrice!!.text = "₹ ${prod.cost.toFloat().roundToInt()}/${prod.unittype.trim().trimEnd('s')}"

                val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/" + prod.itemID + ".jpg")
                GlideApp.with(context!!)
                        .load(storageReference)
                        .error(R.mipmap.prakritifresh_logo)
                        .into(ivSubscriptionItem!!)

                ivSubscriptionOutofstock?.visibility = if (prod.available == "no") View.VISIBLE else View.INVISIBLE

                if (prod.available != "no")
                    f5v?.findViewById<View>(R.id.btnSubscribe)?.setOnClickListener {
                        val shopItem1 = OrderItem(prod)
                        shopItem1.number = 0F

                        SubscriptionViewModel.getActiveSubscriptionId {
                            startActivity(Intent(activity, SubscriptionDateSelectActivity::class.java)
                                    .putExtra("item", shopItem1)
                                    .putExtra("subkey", it))
                        }

                    }
                f5v?.scaleX = 0.9f
                f5v?.scaleY = 0.9f
                return f5v
            }

            override fun getItem(position: Int): Any = position
            override fun getItemId(position: Int): Long = position.toLong()
            override fun getCount(): Int = data.size
        }
    }
}