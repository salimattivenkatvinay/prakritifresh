package com.prakritifresh.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.mrittica.OrderItem
import com.mrittica.SubscriptionOrder
import com.mrittica.constants.OrderTypes
import com.mrittica.utils.convertToString
import com.mrittica.utils.round
import com.mrittica.utils.roundToCurrency
import com.prakritifresh.MyApp.Companion.db
import com.prakritifresh.R
import com.prakritifresh.activities.BillActivity
import com.prakritifresh.base.MyFragment
import com.prakritifresh.pojos.SubscriptionF
import com.prakritifresh.utils.Constants
import kotlinx.android.synthetic.main.fragment_subscription_month.view.*
import kotlinx.android.synthetic.main.row_subscription_monthly.view.*
import java.text.SimpleDateFormat
import java.util.*


class SubscriptionMonthFragment(private val dat: OrderItem, val subscription: SubscriptionF) : MyFragment() {

    var page = 0
    var map: MutableMap<String, SubscriptionOrder> = mutableMapOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_subscription_month, container, false)
        view.next.setOnClickListener {
            if (page < 5) {
                page++
                view.prev.visibility = View.VISIBLE
            }
            if (page == 5) view.next.visibility = View.INVISIBLE
            view.rv_subs.adapter?.notifyDataSetChanged()
        }
        view.prev.setOnClickListener {
            if (page > 0) {
                page--
                view.next.visibility = View.VISIBLE
            }
            if (page == 0)  view.prev.visibility = View.INVISIBLE
            view.rv_subs.adapter?.notifyDataSetChanged()
        }

        FirebaseFirestore.getInstance().collection("orders")
                .whereEqualTo("subkey", subscription.id)
                .get()
                .addOnCompleteListener { task: Task<QuerySnapshot> ->
                    if (task.isSuccessful) {
                        for (documentSnapshot in task.result) {
                            val m = documentSnapshot.toObject(SubscriptionOrder::class.java)
                            m.order_id = documentSnapshot.id
                            map[m.order_id!!.split(":::")[1]] = m
                        }

                        val tot = map.filter { it.key.toDate() <= Date() }
                                .map {
                                    it.value.det?.map { it1 -> (it1.number * it1.cost.toFloat()).roundToCurrency() }
                                            ?.sum() ?: 0f
                                }
                                .sumByDouble { it.toDouble() }

                        if (tot - subscription.paidAmount > 1) {
                            view.tv_reminder.visibility = View.VISIBLE
                        }
                        view.tv_paid.tv_price_rem.text = "Rs. ${tot - subscription.paidAmount!!}"
                        view.tv_paid.tv_tot.text = "Rs. ${tot}"
                        view.tv_paid.tv_paid_amount.text = "Rs. ${subscription.paidAmount!!}"

                        if (!task.result.isEmpty) {
                            view.rv_subs.adapter = SubAdapter()

                            view.pn.setOnClickListener {

                                val shopItem = OrderItem()
                                shopItem.number = 1F
                                shopItem.name = subscription.id
                                shopItem.subcat = "SubscriptionF"
                                shopItem.category = OrderTypes.SUBSCRIPTION_PAYMENT.name
                                shopItem.cost = "${tot - subscription.paidAmount}"

                                startActivity(Intent(activity, BillActivity::class.java)
                                        .putExtra(Constants.CART_LIST, arrayListOf(shopItem))
                                        .putExtra(Constants.orderType, OrderTypes.SUBSCRIPTION_PAYMENT.name))

                            }
                        } else Log.i(Constants.TAG, "empty")
                    } else Log.i(Constants.TAG, task.exception!!.message)
                }
        return view
    }

    inner class SubAdapter : RecyclerView.Adapter<SubAdapter.SubItemHolder>() {

        public inner class SubItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            init {

            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
                SubItemHolder(LayoutInflater.from(context).inflate(R.layout.row_subscription_monthly, parent, false))

        override fun onBindViewHolder(holder: SubItemHolder, position: Int) {
            val cur = addDays(subscription.startDate!!, position + page * 5)

            if (map[cur.time.convertToString(Constants.dateFormat)]?.det?.get(0)?.number ?: 0f > 0f)
                holder.itemView.flag1.setImageDrawable(TextDrawable.builder()
                        .buildRound((cur.time.convertToString("dd")), resources.getColor(R.color.quantum_googgreen200)))
            else
                holder.itemView.flag1.setImageDrawable(TextDrawable.builder()
                        .buildRoundRect((cur.time.convertToString("dd")), resources.getColor(R.color.quantum_grey100), 100))

            holder.itemView.namecart.text = cur.time.convertToString("MMM")
            holder.itemView.tv_status.text = "status :" + (map[cur.time.convertToString(Constants.dateFormat)]?.status
                    ?: "No subscription")
            holder.itemView.pricecart.text = "Rs. " + dat.cost + "/ " + (dat.unittype.trim { it <= ' ' }.trimEnd('s'))

            holder.itemView.nfunit.text = (map[cur.time.convertToString(Constants.dateFormat)]?.det?.get(0)?.number?.toString()
                    ?: "0")

            if (cur.time - System.currentTimeMillis() > 9 * 60 * 60 * 1000) {
                holder.itemView.removeit.visibility = View.VISIBLE
                holder.itemView.addit.visibility = View.VISIBLE

                holder.itemView.removeit.setOnClickListener {
                    var i = holder.itemView.nfunit.text.toString().toFloat().round(2)
                    if (i >= dat.unitqty.toFloat()) {
                        i -= dat.unitqty.toFloat().round(2)
                        i = i.roundToCurrency()
                        holder.itemView.nfunit.text = String.format("%.2f", i)
                        map[cur.time.convertToString(Constants.dateFormat)]?.det?.get(0)?.number = i
                        val pd = ProgressDialog(context)
                        pd.setTitle("Updating qty...")
                        pd.setCanceledOnTouchOutside(false)
                        pd.show()
                        db.collection("orders")
                                .document(map[cur.time.convertToString(Constants.dateFormat)]?.order_id!!)
                                .update("det", map[cur.time.convertToString(Constants.dateFormat)]?.det)
                                .addOnCompleteListener { task: Task<Void?> ->
                                    pd.dismiss()
                                    if (!task.isSuccessful) {
                                        Toast.makeText(context, task.exception!!.message, Toast.LENGTH_SHORT).show()
                                        Log.d("hhh", "onComplete: " + task.exception)
                                        notifyDataSetChanged()
                                    }
                                }
                    }
                }
                holder.itemView.addit.setOnClickListener {
                    var i = holder.itemView.nfunit.text.toString().toFloat() + dat.unitqty.toFloat().round(2)
                    i = i.roundToCurrency()
                    holder.itemView.nfunit.text = String.format("%.2f", i)
                    map[cur.time.convertToString(Constants.dateFormat)]?.det?.get(0)?.number = i
                    val pd = ProgressDialog(context)
                    pd.setTitle("Updating qty...")
                    pd.setCanceledOnTouchOutside(false)
                    pd.show()
                    db.collection("orders")
                            .document(map[cur.time.convertToString(Constants.dateFormat)]?.order_id!!)
                            .update("det", map[cur.time.convertToString(Constants.dateFormat)]?.det)
                            .addOnCompleteListener { task: Task<Void?> ->
                                pd.dismiss()
                                if (!task.isSuccessful) {
                                    Toast.makeText(context, task.exception!!.message, Toast.LENGTH_SHORT).show()
                                    Log.d("hhh", "onComplete: " + task.exception)
                                }
                            }
                }
            } else {
                holder.itemView.removeit.visibility = View.GONE
                holder.itemView.addit.visibility = View.GONE
            }
        }

        override fun getItemCount() = 5
    }

    private fun addDays(dt: Date, days: Int): Date {
        val c = Calendar.getInstance()
        c.time = dt
        c.add(Calendar.DATE, days)
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MILLISECOND, 0)
        return c.time
    }

    private fun String.toDate(): Date =
            SimpleDateFormat(Constants.dateFormat).parse(this)

}