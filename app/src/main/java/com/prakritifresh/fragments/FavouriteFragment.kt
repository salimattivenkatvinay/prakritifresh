package com.prakritifresh.fragments

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.prakritifresh.R
import com.prakritifresh.activities.HomeActivity
import com.prakritifresh.adapters.FavouritesAdapter
import com.prakritifresh.utils.SharedPrefsUtil

class FavouriteFragment : Fragment() {
    private var grid: GridView? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favourite, null)
        grid = view.findViewById(R.id.gridview1)
        return view
    }

    override fun onResume() {
        super.onResume()
        if (SharedPrefsUtil.getInstance(context).likesSet.size > 0) {
            val adapter = FavouritesAdapter(context!!, activity as HomeActivity?)
            grid?.adapter = adapter
        } else {
            val toast = Toast.makeText(context, "no favourites", Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
            toast.show()
        }
    }
}