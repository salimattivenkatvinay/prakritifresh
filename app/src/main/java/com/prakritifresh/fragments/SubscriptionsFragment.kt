package com.prakritifresh.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.prakritifresh.R
import com.prakritifresh.utils.Constants.TAG
import com.prakritifresh.utils.SharedPrefsUtil
import kotlinx.android.synthetic.main.fragment_subscriptions.view.*


class SubscriptionsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_subscriptions, container, false)
        if (SharedPrefsUtil.getInstance(requireContext()).pincode in FirebaseRemoteConfig.getInstance().getString("subscription_enabled_pins").toStringsList()) {
            view.fl.visibility = View.VISIBLE
            view.tv.visibility = View.GONE
            view.vp_subscription.adapter = object : FragmentStatePagerAdapter(childFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
                override fun getItem(position: Int): Fragment = when (position) {
                    0 -> SubscriptionItemsFragment()
                    1 -> SubscriptionOrdersFragment()
                    else -> SubscriptionItemsFragment()
                }

                override fun getCount(): Int = 2
                override fun getPageTitle(position: Int): CharSequence? = when (position) {
                    0 -> "Active"

                    1 -> "Subscriptions"
                    else -> null
                }
            }
            view.tl_subscription.setupWithViewPager(view.vp_subscription)
        }
        return view
    }

    private fun String.toStringsList() : List<String> {
        Log.i(TAG, "toStringsList: $this")
        return Gson().fromJson(this, List::class.java) as List<String>;
    }
}
