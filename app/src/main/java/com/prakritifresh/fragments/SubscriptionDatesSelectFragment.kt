package com.prakritifresh.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.WriteBatch
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import com.mrittica.OrderItem
import com.mrittica.SubscriptionOrder
import com.mrittica.constants.OrderStatus
import com.mrittica.constants.OrderTypes
import com.mrittica.utils.convertToString
import com.mrittica.utils.roundToCurrency
import com.prakritifresh.GlideApp
import com.prakritifresh.MyApp
import com.prakritifresh.MyApp.Companion.db
import com.prakritifresh.R
import com.prakritifresh.activities.SubscriptionListener
import com.prakritifresh.pojos.SubscriptionF
import com.prakritifresh.utils.Constants
import com.prakritifresh.viewModels.SubscriptionViewModel
import kotlinx.android.synthetic.main.fragment_subscription_dates_select.*
import kotlinx.android.synthetic.main.fragment_subscription_dates_select.view.*
import kotlinx.android.synthetic.main.row_cart.view.*
import java.util.*


class SubscriptionDatesSelectFragment(private var subscriptionListener: SubscriptionListener, private val dat: OrderItem, val subscription: SubscriptionF) : Fragment(), View.OnClickListener {

    val subKey = subscription.id!!
    private val selectedDates = ArrayList<String>()
    private val filledDates = ArrayList<String>()
    private var startDate = addDays(Date(), 1)
    private var endDate = subscription.endDate!!
    private var mem = SparseArray<Date>()
    private lateinit var view1: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view1 = inflater.inflate(R.layout.fragment_subscription_dates_select, container, false)
        view1.btnSubscribe.setOnClickListener {
            subscribe()
        }
        view1.cart1.visibility = View.INVISIBLE
        view1.available_qty.visibility = View.INVISIBLE
        return view1
    }

    override fun onResume() {
        super.onResume()
        populateDatesLayout()

        view1.nfunit.text = String.format("%.2f", dat.number)
        view1.namecart.text = (if (dat.subcat.equals("main", ignoreCase = true)) "" else dat.subcat + "-") + dat.name
        view1.pricecart.text = dat.cost + "/ " + dat.unittype.trim().trimEnd { it == 's' }
        val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/" + dat.itemID + ".jpg")

        GlideApp.with(context!!)
                .load(storageReference)
                .error(R.mipmap.prakritifresh_logo)
                .into(view1.flag1)

        view1.flag1.setBackgroundColor(0)
        view1.txtqt1.text = dat.number.toString() + " " + dat.unittype
        view1.removeit.setOnClickListener {
            var i: Float = view1.nfunit.text.toString().toFloat()
            if (i > dat.unitqty.toFloat()) {
                i -= dat.unitqty.toFloat()
                dat.number = i
            }
            view1.nfunit.text = String.format("%.2f", i)
            view1.txtqt1.text = String.format("%.2f", i) + " " + dat.unittype
        }
        view1.addit.setOnClickListener {
            val i: Float = view1.nfunit.text.toString().toFloat() + dat.unitqty.toFloat()
            dat.number = i
            view1.nfunit.text = String.format("%.2f", i)
            view1.txtqt1.text = String.format("%.2f", i) + " " + dat.unittype
        }

        btnDaily.setOnClickListener {
            btnClear.performClick()
            btnDaily.setBackgroundResource(R.drawable.btn_green_curved)
            btnAltDaily.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_border, null)
            btnManual.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_border, null)

//            Toast.makeText(context, "daily", Toast.LENGTH_SHORT).show()
            var td = Date(startDate.time)
            while (td <= endDate) {
                view1.findViewWithTag<TextView>(td.time.convertToString(Constants.format)).apply {
                    setBackgroundResource(R.drawable.blue_circle)
                }
                selectedDates.add(td.time.convertToString(Constants.format))
                td = addDays(td, 1)
            }
        }

        btnAltDaily.setOnClickListener {
            btnClear.performClick()
            btnDaily.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_border, null)
            btnAltDaily.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_curved, null)
            btnManual.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_border, null)

//            Toast.makeText(context, "altdaily", Toast.LENGTH_SHORT).show()
            var td = Date(startDate.time)
            while (td <= endDate) {
                view1.findViewWithTag<TextView>(td.time.convertToString(Constants.format)).apply {
                    setBackgroundResource(R.drawable.blue_circle)
                }
                selectedDates.add(td.time.convertToString(Constants.format))
                td = addDays(td, 2)
            }
        }

        btnClear.setOnClickListener {

            btnDaily.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_border, null)
            btnAltDaily.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_border, null)
            btnManual.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_curved, null)

            selectedDates.forEach {
                view1.findViewWithTag<TextView>(it).apply {
                    background = null
                }
            }
            selectedDates.clear()
        }
    }

    private fun populateDatesLayout() {

        var d = Date()
        val start = d.day
        d = addDays(d, -start)

        Log.i(Constants.TAG, "$start : $d")

        for (i in 1..42) {
            val t = TextView(context)
            t.id = i
            t.tag = d.time.convertToString(Constants.format)
            when (i) {
                in 1..7 -> view1.LlRow1.addView(t)
                in 8..14 -> view1.LlRow2.addView(t)
                in 15..21 -> view1.LlRow3.addView(t)
                in 22..28 -> view1.LlRow4.addView(t)
                in 29..35 -> view1.LlRow5.addView(t)
                in 36..42 -> view1.LlRow6.addView(t)
            }
            t.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.FILL_PARENT, 1f)
            t.gravity = Gravity.CENTER

            t.text = "" + d.date
            mem.put(i, d)
            if (filledDates.contains(d.time.convertToString(Constants.format)) || d.time < startDate.time || d.time > endDate.time) {
                t.setBackgroundResource(R.drawable.grey_circle)
                t.setTextColor(Color.LTGRAY)
            }
            d = addDays(d, 1)
            t.setOnClickListener(this)
        }
    }

    override fun onClick(v: View?) {
        if (v is TextView) {
            when (mem.get(v.id)) {
                null -> null
                else -> {
                    val clicked = v.id
                    if (filledDates.contains(mem.get(clicked).time.convertToString(Constants.format)) || mem.get(clicked).time < startDate.time || mem.get(clicked).time > endDate.time) {
//                        Toast.makeText(applicationContext, "slot not available", Toast.LENGTH_SHORT).show()
                    } else {

                        btnDaily.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_border, null)
                        btnAltDaily.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_border, null)
                        btnManual.background = ResourcesCompat.getDrawable(resources, R.drawable.btn_green_curved, null)

                        if (selectedDates.contains(mem.get(clicked).time.convertToString(Constants.format))) {
                            selectedDates.remove(mem.get(clicked).time.convertToString(Constants.format))
                            v.background = null
                        } else {
                            selectedDates.add(mem.get(clicked).time.convertToString(Constants.format))
                            v.setBackgroundResource(R.drawable.blue_circle)
                        }
                    }
                }
            }
        }
    }

    private fun subscribe() {
        if (selectedDates.size == 0) {
            Toast.makeText(context, "no slot selected", Toast.LENGTH_SHORT).show()
            return
        } else if (dat.number == 0f) {
            Toast.makeText(context, "select some quantity", Toast.LENGTH_SHORT).show()
            return
        } else {
            if (subscription == null) return
            subscription.itemsSubscribed.add(dat.itemID!!)
            val gson = Gson()
            val batch: WriteBatch = db.batch()
            for (o in 0..41) {
                val clicked = mem.keyAt(o)
                if (filledDates.contains(mem.get(clicked).time.convertToString(Constants.format)) || mem.get(clicked).time < startDate.time || mem.get(clicked).time > endDate.time)
                    continue

                val deepCopy: OrderItem = gson.fromJson(gson.toJson(dat), OrderItem::class.java)
                if (!selectedDates.contains(mem.get(clicked).time.convertToString(Constants.format)))
                    deepCopy.number = 0f
                val order = SubscriptionOrder()
                order.user_id = FirebaseAuth.getInstance().uid
                order.userName = FirebaseAuth.getInstance().currentUser?.displayName
                order.det = listOf(deepCopy)
                order.address = subscription.address
                order.landmark = subscription.landmark
                order.phno = subscription.phno
                val c = Calendar.getInstance()
                c.time = mem.get(clicked)
                c.set(Calendar.HOUR_OF_DAY, 7)
                order.orderedTime = c.timeInMillis
                c.set(Calendar.HOUR_OF_DAY, 10)
                order.scheduledTime = c.timeInMillis
                order.status = OrderStatus.PENDING
                order.amount = (dat.number * dat.cost.toFloat().roundToCurrency())
                order.shop_id = dat.shop
                order.lat = subscription.lat!!
                order.lng = subscription.lon!!
                order.delivery = 0f
                order.discount = 0f
                order.subkey = subKey
                order.orderType = OrderTypes.SUBSCRIPTION.name
                order.payementMode = OrderTypes.SUBSCRIPTION.name

                val nycRef: DocumentReference = db.collection("orders").document(subKey + ":::" + mem.get(clicked).time.convertToString(Constants.dateFormat))
                batch.set(nycRef, order)
            }
            MyApp.db.collection(Constants.subscriptions).document(subKey)
                    .set(subscription, SetOptions.merge())
                    .addOnSuccessListener {
                        SubscriptionViewModel.setIsCached()
                    }
            batch.commit().addOnCompleteListener {
                if (it.isSuccessful) {
                    subscription.itemsSubscribed.add(dat.itemID!!)
                    MyApp.db.collection(Constants.subscriptions).document(subKey)
                            .set(subscription, SetOptions.merge())
                            .addOnCompleteListener {
                                subscriptionListener.onSubscribe()
                            }
                } else
                    Toast.makeText(context, it.exception?.localizedMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun addDays(dt: Date, days: Int): Date {
        val c = Calendar.getInstance()
        c.time = dt
        c.add(Calendar.DATE, days)
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MILLISECOND, 0)
        return c.time
    }
}