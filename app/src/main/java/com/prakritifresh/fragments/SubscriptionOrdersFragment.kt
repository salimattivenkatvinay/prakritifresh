package com.prakritifresh.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mrittica.OrderItem
import com.mrittica.utils.convertToString
import com.prakritifresh.R
import com.prakritifresh.pojos.SubscriptionF
import com.prakritifresh.utils.Constants
import com.prakritifresh.viewModels.SubscriptionViewModel
import kotlinx.android.synthetic.main.row_subscription_order.view.*
import java.util.*

class SubscriptionOrdersFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private val order_items = ArrayList<SubscriptionF>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_subscription_orders, container, false)
        recyclerView = view.findViewById(R.id.rv_orders)
        recyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onResume() {
        super.onResume()
        if (recyclerView.adapter != null) {
            order_items.clear()
            recyclerView.adapter?.notifyDataSetChanged()
        }
        val loading = ProgressDialog(context)
        loading.setMessage(Html.fromHtml("<b><h2>Fetching orders...."))
        loading.show()
        //loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false)

        SubscriptionViewModel.getSubscriptions {
            loading.dismiss()
            order_items.addAll(it)
            if (order_items.size > 0)
                recyclerView.adapter = OrderDetAdapter()
        }
    }

    private inner class OrderDetAdapter : RecyclerView.Adapter<OrderDetAdapter.OrderDetHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDetHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.row_subscription_order, parent, false)
            return OrderDetHolder(view)
        }

        override fun onBindViewHolder(holder: OrderDetHolder, position: Int) {
            holder.tv_order_id.text = Html.fromHtml("Order Id: <i>" + order_items.get(position).id.toString() + "</i>")
            val amount: Float? = order_items.get(position).paidAmount
            holder.tv_price.text = "Rs. $amount"
            holder.itemView.tv_tot.text = ""
            holder.tvdates.text = "Subscribed : ${order_items.get(position).startDate?.time?.convertToString(Constants.dateFormat)} - ${order_items.get(position).endDate?.time?.convertToString(Constants.dateFormat)}"
            holder.tv_placed_on.text = "Subscribed on " + order_items.get(position).startDate!!.time.convertToString("EEE, MMM d, hh:mm aaa")
            holder.btn_view_details.setOnClickListener {
                val shopItem = OrderItem()
                shopItem.number = 1F
                shopItem.subcat = "Plus membership"
                shopItem.cost = if (Calendar.getInstance().get(Calendar.YEAR) == 2019) "30" else "90"

                /*   startActivity(Intent(activity, SubscriptionDateSelectActivity::class.java)
                           .putExtra(Constants.CART_LIST, arrayListOf(shopItem, shopItem))
                           .putExtra(Constants.orderType, Constants.subscription))*/
            }
        }

        override fun getItemCount(): Int {
            return order_items.size
        }

        inner class OrderDetHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tv_placed_on: TextView
            var tv_price: TextView
            var tv_order_id: TextView
            var tvdates: TextView
            var btn_view_details: Button

            init {
                tv_placed_on = itemView.findViewById(R.id.tv_placed_on)
                tv_price = itemView.findViewById(R.id.tv_paid_amount)
                tvdates = itemView.findViewById(R.id.tvDates)
                tv_order_id = itemView.findViewById(R.id.tv_order_id)
                btn_view_details = itemView.findViewById(R.id.btn_view_details)
            }
        }
    }
}