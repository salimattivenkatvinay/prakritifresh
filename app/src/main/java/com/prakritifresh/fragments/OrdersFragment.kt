package com.prakritifresh.fragments

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.faltenreich.skeletonlayout.Skeleton
import com.faltenreich.skeletonlayout.applySkeleton
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.mrittica.Order
import com.mrittica.constants.OrderStatus
import com.mrittica.constants.OrderTypes
import com.mrittica.utils.convertToString
import com.mrittica.utils.toTitleCase
import com.prakritifresh.R
import com.prakritifresh.activities.OrderDetailsActivity
import com.prakritifresh.activities.PaymentActivity
import com.prakritifresh.utils.Constants
import java.util.*

class OrdersFragment : Fragment(), OrderStatus {
    private var recyclerView: RecyclerView? = null
    private val order_items = ArrayList<Order?>()
    private var skeleton: Skeleton? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_orderitem_list, container, false)
        recyclerView = view.findViewById(R.id.rv_orders)
        recyclerView?.setLayoutManager(LinearLayoutManager(context))
        return view
    }

    override fun onResume() {
        super.onResume()
        if (recyclerView!!.adapter != null) {
            order_items.clear()
            recyclerView!!.adapter!!.notifyDataSetChanged()
        }
        /*   final ProgressDialog loading;
        loading = new ProgressDialog(getContext());
        loading.setMessage(Html.fromHtml("<b><h2>Fetching orders...."));
        loading.show();
        //loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);*/skeleton =
            recyclerView!!.applySkeleton(R.layout.row_order_skeleton, 5)
        skeleton!!.showSkeleton()
        FirebaseFirestore.getInstance().collection("orders")
            .whereEqualTo("user_id", FirebaseAuth.getInstance().uid)
//            .whereLessThan(
//                "orderedTime",
//                System.currentTimeMillis()
//            )
            //                .orderBy("orderedTime", Query.Direction.DESCENDING)
            .get()
            .addOnCompleteListener { task: Task<QuerySnapshot> ->
//                    loading.hide();
                skeleton!!.showOriginal()
                if (task.isSuccessful) {
                    for (documentSnapshot in task.result) {
                        val m = documentSnapshot.toObject(Order::class.java)
                        m.order_id = documentSnapshot.id
                        Log.i(Constants.TAG, "orders list: $m")
                        if (m.orderedTime < System.currentTimeMillis())
                        order_items.add(m)
                    }
                    if (!task.result.isEmpty) {
                        recyclerView!!.adapter = OrderDetAdapter(order_items.filterNotNull().sortedBy { it.orderedTime })
                    } else
                        Log.i(Constants.TAG, "empty")
                } else
                    Log.i(Constants.TAG, task.exception!!.message)
            }
    }

    internal inner class OrderDetAdapter(private val order_items: List<Order>) : RecyclerView.Adapter<OrderDetAdapter.OrderDetHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDetHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.row_order, parent, false)
            return OrderDetHolder(view)
        }

        override fun onBindViewHolder(holder: OrderDetHolder, position: Int) {
            val order = order_items[position]
            holder.tv_order_id.text = Html.fromHtml("Order Id: <i>" + order!!.order_id + "</i>")
            val amount = order.amount
            holder.tv_price.text = "Rs. $amount"
            if (order.delivery == 0f || order.orderType == OrderTypes.SUBSCRIPTION.name) {
                holder.tv_final_price.text = "Rs. $amount"
                holder.tv_delivery_price.text = "FREE"
                holder.tv_delivery_price.setTextColor(resources.getColor(R.color.green))
            } else {
                holder.tv_delivery_price.text = "Rs. " + order.delivery
                holder.tv_final_price.text = "Rs. " + (amount + order.delivery)
                holder.tv_delivery_price.setTextColor(Color.RED)
            }
            holder.tv_status.text = order.status.toTitleCase()
            holder.tv_placed_on.text =
                "Placed on " + order.orderedTime.convertToString(Constants.ordersDateFormat)
            holder.btn_view_details.setOnClickListener { v: View? ->
                startActivity(
                    Intent(
                        context, OrderDetailsActivity::class.java
                    )
                        .putExtra("order", order)
                )
            }
            if (order.status == OrderStatus.DELIVERED) {
                holder.tv_scheduled_for.text =
                    "Delivered on " + order.deliveredTime.convertToString(
                        Constants.ordersDateFormat
                    )
                holder.tv_paid_using.visibility = View.VISIBLE
                holder.btn_pay_now.visibility = View.GONE
                holder.tv_paid_using.text = "Paid using : " + order.payementMode
            } else {
                if (order.payementMode == null) {
                    holder.tv_paid_using.visibility = View.GONE
                    holder.btn_pay_now.visibility = View.VISIBLE
                } else {
                    holder.tv_paid_using.visibility = View.VISIBLE
                    holder.btn_pay_now.visibility = View.GONE
                    holder.tv_paid_using.text = "Paid using : " + order.payementMode
                }
                holder.tv_scheduled_for.text =
                    "Scheduled for " + order.scheduledTime.convertToString(
                        Constants.ordersDateFormat
                    )
            }
            holder.btn_pay_now.setOnClickListener { v: View? ->
                startActivity(
                    Intent(
                        context, PaymentActivity::class.java
                    )
                        .putExtra("orderId", order.order_id)
                        .putExtra("amount", order.amount.toString())
                        .putExtra("shopID", order.shop_id)
                        .putExtra("type", "orders")
                )
            }
        }

        override fun getItemCount(): Int {
            return order_items.size
        }

        internal inner class OrderDetHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tv_placed_on: TextView
            var tv_scheduled_for: TextView
            var tv_price: TextView
            var tv_delivery_price: TextView
            var tv_order_id: TextView
            var tv_status: TextView
            var tv_final_price: TextView
            var tv_paid_using: TextView
            var btn_view_details: Button
            var btn_pay_now: Button

            init {
                tv_placed_on = itemView.findViewById(R.id.tv_placed_on)
                tv_scheduled_for = itemView.findViewById(R.id.tv_scheduled_for)
                tv_price = itemView.findViewById(R.id.tv_price)
                tv_delivery_price = itemView.findViewById(R.id.tv_delivery_price)
                tv_order_id = itemView.findViewById(R.id.tv_order_id)
                tv_status = itemView.findViewById(R.id.tv_status)
                tv_final_price = itemView.findViewById(R.id.tv_final_price)
                tv_paid_using = itemView.findViewById(R.id.tv_paid_using)
                btn_view_details = itemView.findViewById(R.id.btn_view_details)
                btn_pay_now = itemView.findViewById(R.id.btn_pay_now)
            }
        }
    }

    fun method(str: String?): String? {
        var str = str
        if (str != null && str.length > 0 && str[str.length - 1] == 's') {
            str = str.substring(0, str.length - 1)
        }
        return str
    }
}