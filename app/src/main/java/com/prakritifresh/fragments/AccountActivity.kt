package com.prakritifresh.fragments

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.View.OnClickListener
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.mrittica.utils.toTitleCase
import com.prakritifresh.GlideApp
import com.prakritifresh.R
import com.prakritifresh.activities.*
import com.prakritifresh.viewModels.ShopItemsViewModel
import com.prakritifresh.viewModels.SubscriptionViewModel
import java.io.ByteArrayOutputStream


class AccountActivity : AppCompatActivity() {

    lateinit var data: Map<String, Any>

    private var tv_email: TextView? = null
    private var uname: TextView? = null
    private var profession: TextView? = null
    private var gender: TextView? = null
    private var tv_address: TextView? = null
    private var tv_pin: TextView? = null
    private var tv_landmark: TextView? = null
    private var profile_image: ImageView? = null
    private val profileref = FirebaseStorage.getInstance()
            .reference.child("profile_image/" + FirebaseAuth.getInstance().uid + ".jpg")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_account)
        uname = findViewById(R.id.uname)
        profession = findViewById(R.id.profession)
        gender = findViewById(R.id.gender)
        tv_email = findViewById(R.id.tv_email)
        profile_image = findViewById(R.id.profile_image)
        val tv_phno = findViewById<TextView>(R.id.tv_phno)
        tv_phno.text = "Mobile No: " + FirebaseAuth.getInstance().currentUser?.phoneNumber

        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        profile_image?.layoutParams?.height = resources.displayMetrics.widthPixels / 3

        GlideApp.with(this)
                .load(profileref)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .error(R.mipmap.prakritifresh_logo)
                .circleCrop()
                .into(profile_image!!)

        tv_address = findViewById(R.id.input_street)
        tv_landmark = findViewById(R.id.input_landmark)
        tv_pin = findViewById(R.id.input_pincode)
        accountDetails
        findViewById<View>(R.id.btn_logout).setOnClickListener {
            SubscriptionViewModel.setIsCached()
            ShopItemsViewModel.resetCache()
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, SplashScreenActivity::class.java))
            finishAffinity()
        }
        findViewById<View>(R.id.btn_cp).setOnClickListener {
            ImagePicker.with(this@AccountActivity)
                    .crop(1f, 1f)
                    .compress(256)
                    .maxResultSize(220, 220)
                    .start { resultCode, data ->
                        when (resultCode) {
                            Activity.RESULT_OK -> {
                                val fileUri = data?.data
                                profile_image?.setImageURI(fileUri)

                                val bitmap: Bitmap?
                                if (Build.VERSION.SDK_INT < 28) {
                                    bitmap = MediaStore.Images.Media.getBitmap(
                                            this?.contentResolver,
                                            fileUri
                                    )
                                    profile_image?.setImageBitmap(bitmap)
                                } else {
                                    val source = ImageDecoder.createSource(this?.contentResolver!!, fileUri!!)
                                    bitmap = ImageDecoder.decodeBitmap(source)
                                    profile_image?.setImageBitmap(bitmap)
                                }
                                val baos = ByteArrayOutputStream()
                                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                                val data1 = baos.toByteArray()
                                val uploadTask = profileref.putBytes(data1)
                                val loader = ProgressDialog(this)
                                loader.setTitle("Uploading image...")
                                loader.show()
                                uploadTask
                                        .addOnFailureListener { exception: Exception ->
                                            loader.hide()
                                            Toast.makeText(this, exception.localizedMessage, Toast.LENGTH_SHORT).show()
                                        }
                                        .addOnSuccessListener {
                                            loader.hide()
                                            Toast.makeText(this, "success", Toast.LENGTH_SHORT).show()
                                        }
                            }
                            ImagePicker.RESULT_ERROR -> {
                                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                            }
                            else -> {
                                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
        }

        val startMap = OnClickListener { startActivityForResult(Intent(this, MapsActivity::class.java), GET_ADDRESS) }
        findViewById<View>(R.id.iv_ead).setOnClickListener(startMap)
        val startSignUP = OnClickListener { startActivity(Intent(this, SignupActivity::class.java)) }
        findViewById<View>(R.id.iv_eap).setOnClickListener(startSignUP)
        findViewById<View>(R.id.btn_about).setOnClickListener { startActivity(Intent(this, LinksActivity::class.java)) }
        findViewById<View>(R.id.btn_share).setOnClickListener { shareApp() }

        findViewById<View>(R.id.ib_phone).setOnClickListener { startActivity(Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + "+919800073028"))) }
        findViewById<View>(R.id.ib_whatsapp).setOnClickListener { startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=919800073028&text=Query:"))) }
    }

    private val accountDetails: Unit
        get() {
            val cu = FirebaseAuth.getInstance().currentUser
            if (cu == null) {
                startActivity(Intent(this, LoginActivity::class.java))
                return
            } else
                FirebaseFirestore.getInstance().collection("Users")
                        .document(cu.uid)
                        .get()
                        .addOnCompleteListener { task: Task<DocumentSnapshot> ->
                            tv_address!!.text = ""
                            if (task.isSuccessful) {
                                if (!task.result!!.exists()) return@addOnCompleteListener
                                val jsonObject = task.result!!.data
                                data = task.result!!.data!!
                                (data as MutableMap<String, Any>).remove("token")

                                if (jsonObject!!.containsKey("uname")) {
                                    uname!!.text = jsonObject["uname"].toString().toTitleCase()
                                    FirebaseAuth.getInstance().currentUser?.updateProfile(UserProfileChangeRequest.Builder()
                                            .setDisplayName(jsonObject["uname"].toString().toTitleCase())
                                            .build())?.addOnCanceledListener {
                                        Toast.makeText(this, uname?.text.toString().toTitleCase(), Toast.LENGTH_SHORT).show()
                                    }
                                }
                                if (jsonObject.containsKey("profession")) {
                                    profession?.text = jsonObject["profession"].toString().toTitleCase()
                                }
                                if (jsonObject.containsKey("gender")) {
                                    gender?.text = jsonObject["gender"].toString()
                                }
                                if (jsonObject.containsKey("mail")) {
                                    tv_email?.text = "Email ID: " + jsonObject["mail"].toString()
                                }
                                if (jsonObject.containsKey("plot")) {
                                    tv_address!!.append(jsonObject["plot"].toString() + ", ")
                                }
                                if (jsonObject.containsKey("street")) {
                                    tv_address!!.append(jsonObject["street"].toString() + ", ")
                                }
                                if (jsonObject.containsKey("landmark")) {
                                    tv_landmark!!.text = "Landmark: " + jsonObject["landmark"].toString()
                                }
                                if (jsonObject.containsKey("city")) {
                                    tv_address!!.append(jsonObject["city"].toString() + ", ")
                                }
                                if (jsonObject.containsKey("state")) {
                                    tv_address!!.append(jsonObject["state"].toString())
                                }
                                if (jsonObject.containsKey("pin")) {
                                    tv_pin!!.text = "PIN: " + jsonObject["pin"].toString()
                                }
                            } else {
                                Toast.makeText(this, task.exception!!.localizedMessage, Toast.LENGTH_SHORT).show()
                            }
                        }
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GET_ADDRESS) {
            accountDetails
        }
    }

    companion object {
        private const val GET_ADDRESS = 123
    }

    private fun shareApp() {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this organic e-commerce app at: https://play.google.com/store/apps/details?id=com.prakritifresh.user1")
        sendIntent.type = "text/plain"
        startActivity(sendIntent)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Animatoo.animateSlideLeft(this)  //fire the zoom animation
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}