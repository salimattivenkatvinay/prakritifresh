package com.prakritifresh.utils

data class AppVersionStatuses(val active: Int, val current: Int, val deprecated: Int, val disabled: Int) {

    constructor() : this(0, 0, 0, 0)
}