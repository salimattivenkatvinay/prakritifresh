package com.prakritifresh.utils;

public interface Constants {

    String TAG = "venkat_vinay";

    String appName = "Prakritifresh";

    String db = "db";
    String LIKED = "like";
    String CART_LIST = "cartlist";
    String address = "address";
    String scheduledTime = "scheduledTime";
    String cartKey = "cartKey1";
    String pincode = "pincode";
    String region = "region";
    String topic = "user";
    String isPlus = "isPlus";
    String lastSubscribedDate = "lastSubscribedDate";
    String lastSubscribedId = "lastSubscribedId";

    String orderType = "orderType";
    String subscription = "subscription";
    String oneTime = "oneTime";

    String format = "yyyy-MM-dd";
    String dateFormat = "dd-MM-yyyy";
    String ordersDateFormat = "EEE, MMM d YYYY, hh:mm aaa";

    String VEGETABLES = "vegetable";
    String FRUITS = "fruits";
    String STAPLES = "groceries";

    String SUCESS = "sucess";
    String FAILURE = "failure";

    String subscriptions = "subscriptions";
}
