package com.prakritifresh.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class SharedPrefsUtil implements Constants {

    private SharedPreferences sharedPreferences;
    private Gson gson = new Gson();

    private static SharedPrefsUtil single_instance = null;

    private SharedPrefsUtil(Context context) {
        sharedPreferences = context.getSharedPreferences(db, Context.MODE_PRIVATE);
    }

    public static SharedPrefsUtil getInstance(Context context) {
        if (single_instance == null)
            single_instance = new SharedPrefsUtil(context);
        return single_instance;
    }

    public HashMap<String, Float> getCart() {
        HashMap<String, Float> cart1;
        String cartString = sharedPreferences.getString(cartKey, "");
        if (cartString.equals(""))
            cart1 = new HashMap<String, Float>();
        else
            cart1 = gson.fromJson(cartString, new TypeToken<HashMap<String, Float>>() {
            }.getType());
        return cart1;
    }

    public boolean isCartEmpty() {
        return sharedPreferences.getString(cartKey, "").equals("");
    }

    public int getCartSize() {
        return getCart().size();
    }

    public void addToCart(String prodId, Float qty) {
        HashMap<String, Float> cart1 = getCart();
        cart1.put(prodId, qty);
        sharedPreferences.edit()
                .putString(cartKey, gson.toJson(cart1))
                .apply();
    }

    public void removeFromCart(String prodId) {
        HashMap<String, Float> cart1 = getCart();
        cart1.remove(prodId);
        sharedPreferences.edit()
                .putString(cartKey, gson.toJson(cart1))
                .apply();
    }

    public Float getCartItemQty(String prodId) {
        return getCart().get(prodId);
    }

    public void addCartItemQty(String prodId, Float qty) {
        addToCart(prodId, getCartItemQty(prodId) + qty);
    }

    public void minusCartItemQty(String prodId, Float qty) {
        addToCart(prodId, getCartItemQty(prodId) - qty);
    }

    public void clearCart() {
        sharedPreferences.edit()
                .putString(cartKey, "")
                .apply();
    }

    public boolean isInCart(String prodId) {
        return getCart().containsKey(prodId);
    }

    public Set<String> getLikesSet() {
        return sharedPreferences.getStringSet(LIKED, new HashSet<>());
    }

    public ArrayList<String> getLikesList() {
        return new ArrayList<>(getLikesSet());
    }

    public void addToFavourites(String prodId) {
        Set<String> likeSet = getLikesSet();
        likeSet.add(prodId);
        sharedPreferences.edit()
                .putStringSet(LIKED, likeSet)
                .apply();
    }

    public void removeFromFavourites(String prodId) {
        Set<String> likeSet = getLikesSet();
        likeSet.remove(prodId);
        sharedPreferences.edit()
                .putStringSet(LIKED, likeSet)
                .apply();
    }

    public String getPincode() {
        return sharedPreferences.getString(pincode, "736122");
    }

    public String getRegion() {
        return sharedPreferences.getString(region, "");
    }

    public void changeLocation(String loc, String reg) {
        sharedPreferences.edit()
                .putString(pincode, loc)
                .putString(region, reg)
                .putString(cartKey, "")
                .putStringSet(LIKED, new HashSet<>())
                .apply();
    }

}
