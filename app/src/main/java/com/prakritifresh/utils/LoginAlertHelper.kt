package com.prakritifresh.utils

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.prakritifresh.activities.LoginActivity

object LoginAlertHelper {
    fun createAlert(context: Context) {
        AlertDialog.Builder(context)
                .setTitle("Login to place order")
                .setPositiveButton("Login") { _: DialogInterface?, _: Int -> context.startActivity(Intent(context, LoginActivity::class.java)) }
                .setNegativeButton("cancel") { d: DialogInterface, _: Int -> d.dismiss() }
                .create()
                .show()
    }
}