package com.prakritifresh.activities

import android.app.Activity
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.prakritifresh.R
import kotlinx.android.synthetic.main.activity_replace_request.*
import java.io.ByteArrayOutputStream

class ReplaceRequestActivity : AppCompatActivity() {

    private var imageUploaded: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_replace_request)

        val key = intent.getStringExtra("key")


        val profileref = FirebaseStorage.getInstance()
                .reference.child("replace_request_image/$key.jpg")

        imageView.setOnClickListener {
            ImagePicker.with(this)
                    .crop(1f, 1f)
                    .compress(256)
                    .maxResultSize(480, 480)
                    .start { resultCode, data ->
                        if (resultCode == Activity.RESULT_OK) {
                            val fileUri = data?.data
                            imageView?.setImageURI(fileUri)

                            val bitmap: Bitmap?
                            if (Build.VERSION.SDK_INT < 28) {
                                bitmap = MediaStore.Images.Media.getBitmap(
                                        contentResolver,
                                        fileUri
                                )
                                imageView?.setImageBitmap(bitmap)
                            } else {
                                val source = ImageDecoder.createSource(contentResolver!!, fileUri!!)
                                bitmap = ImageDecoder.decodeBitmap(source)
                                imageView?.setImageBitmap(bitmap)
                            }
                            val baos = ByteArrayOutputStream()
                            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                            val data1 = baos.toByteArray()
                            val uploadTask = profileref.putBytes(data1)
                            val loader = ProgressDialog(this)
                            loader.setTitle("Uploading image...")
                            loader.show()
                            uploadTask
                                    .addOnFailureListener { exception: Exception ->
                                        loader.hide()
                                        Toast.makeText(applicationContext, exception.localizedMessage, Toast.LENGTH_SHORT).show()
                                    }
                                    .addOnSuccessListener {
                                        loader.hide()
                                        imageUploaded = true
                                        Toast.makeText(applicationContext, "success", Toast.LENGTH_SHORT).show()
                                    }
                        } else if (resultCode == ImagePicker.RESULT_ERROR) {
                            Toast.makeText(applicationContext, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(applicationContext, "Task Cancelled", Toast.LENGTH_SHORT).show()
                        }
                    }
        }

        button.setOnClickListener {

            if (editText.text.toString().isBlank()) {
                Toast.makeText(applicationContext, "Enter reason", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!imageUploaded) {
                Toast.makeText(applicationContext, "Pls upload product photo, so we can analyse better", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseFirestore.getInstance()
                    .collection("replaceRequest")
                    .add(hashMapOf(
                            "key" to key,
                            "status" to "PENDING",
                            "reason" to editText.text.toString()
                    ))
            Toast.makeText(applicationContext, "Request raised. We'll reach back to you soon", Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}
