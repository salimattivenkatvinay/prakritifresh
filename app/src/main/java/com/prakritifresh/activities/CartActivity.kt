package com.prakritifresh.activities

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.mrittica.OrderItem
import com.mrittica.ShopItem
import com.mrittica.constants.OrderTypes
import com.mrittica.utils.round
import com.prakritifresh.GlideApp
import com.prakritifresh.R
import com.prakritifresh.utils.Constants
import com.prakritifresh.utils.SharedPrefsUtil
import kotlinx.android.synthetic.main.activity_cart.*
import kotlin.math.roundToInt

class CartActivity : AppCompatActivity() {
    var aList = ArrayList<ShopItem>()
    var adapter: CartAdapter? = null
    var listView: ListView? = null
    var textView: TextView? = null
    var db = FirebaseFirestore.getInstance()
    var count = 0
    var inStock = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        listView = findViewById(R.id.listview_cart)
        textView = findViewById(R.id.noitems)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Cart"
        jSON
        checkout.setOnClickListener {
            when {
                FirebaseAuth.getInstance().currentUser == null -> {
                    AlertDialog.Builder(this)
                            .setTitle("Login to place order")
                            .setPositiveButton("Login") { d: DialogInterface?, w: Int -> startActivity(Intent(applicationContext, LoginActivity::class.java)) }
                            .setNegativeButton("cancel") { d: DialogInterface, w: Int -> d.dismiss() }
                            .create()
                            .show()
                }
                aList.size > 0 -> {
                    val blist: ArrayList<OrderItem> = ArrayList()
                    for (s in aList) {
                        val o = OrderItem(s)
                        o.number = SharedPrefsUtil.getInstance(this).getCartItemQty(s.id)
                        blist.add(o)
                    }
                    startActivity(Intent(this, BillActivity::class.java)
                            .putExtra(Constants.CART_LIST, blist)
                            .putExtra(Constants.orderType, OrderTypes.ONETIME.name))
                }
                else -> Toast.makeText(applicationContext, "First add few items to list", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val jSON: Unit
        get() {
            if (!SharedPrefsUtil.getInstance(this).isCartEmpty) {
                val loading = ProgressDialog(this@CartActivity)
                loading.setMessage(Html.fromHtml("<b><h2>Retrieving data...."))
                loading.show()
                loading.setCanceledOnTouchOutside(false)
                val ids = SharedPrefsUtil.getInstance(this).cart.keys.toTypedArray()
                val tot = ids.size
                aList = ArrayList()
                Log.i(Constants.TAG, ids.contentToString())
                count = 0
                inStock = 0
                if (tot == 0) {
                    loading.hide()
                    textView!!.visibility = View.VISIBLE
                } else {
                    for (id in ids) {
                        db.collection("shopitems").document(id)
                                .get()
                                .addOnCompleteListener { task: Task<DocumentSnapshot> ->
                                    count++
                                    if (task.isSuccessful && task.result!!.exists()) {
                                        val shopItem = task.result!!.toObject(ShopItem::class.java)
                                        shopItem!!.id = task.result!!.id
                                        if (shopItem.available.equals("yes", ignoreCase = true) && shopItem.unitqty.toFloat() <= shopItem.available_quantity) {
                                            inStock++
                                            aList.add(shopItem)
                                        }
                                    }
                                    if (count == tot) {
                                        if (inStock != count) Toast.makeText(applicationContext, (count - inStock).toString() + " Out of stock items excluded", Toast.LENGTH_SHORT).show()
                                        loading.hide()
                                        showJSON()
                                    }
                                }
                    }
                }
            } else {
                textView!!.visibility = View.VISIBLE
            }
        }

    private fun showJSON() {
        adapter = CartAdapter()
        listView!!.adapter = adapter
    }

    inner class CartAdapter : BaseAdapter() {
        private val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        override fun getCount() = aList.size
        override fun getItem(position: Int) = position
        override fun getItemId(position: Int) = position.toLong()
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view = convertView
            if (view == null) {
                view = inflater.inflate(R.layout.row_cart, null)!!
            }
            val dat: ShopItem = aList[position]
            val imageView = view.findViewById<ImageView>(R.id.flag1)
            val txtqty1 = view.findViewById<TextView>(R.id.txtqt1)
            val nam = view.findViewById<TextView>(R.id.namecart)
            val pr = view.findViewById<TextView>(R.id.pricecart)
            val remove = view.findViewById<ImageButton>(R.id.removeit)
            val addi = view.findViewById<ImageButton>(R.id.addit)
            val imageButton = view.findViewById<ImageButton>(R.id.cart1)
            val qty = view.findViewById<TextView>(R.id.nfunit)
            val available_qty = view.findViewById<TextView>(R.id.available_qty)
            available_qty.text = "Available : ${dat.available_quantity.round(2)}"
            qty.text = String.format("%.2f", SharedPrefsUtil.getInstance(this@CartActivity).getCartItemQty(dat.id))
            nam.text = (if (dat.subcat.equals("main", ignoreCase = true)) "" else dat.subcat + "-") + dat.name
            pr.text = dat.cost + "/ " + removeS(dat.unittype.trim { it <= ' ' })
            val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/" + dat.itemID + ".jpg")
            GlideApp.with(this@CartActivity)
                    .load(storageReference)
                    .error(R.mipmap.prakritifresh_logo)
                    .into(imageView)
            imageView.setBackgroundColor(0)
            txtqty1.text = String.format("%.2f", SharedPrefsUtil.getInstance(this@CartActivity).getCartItemQty(dat.id) * dat.cost.toFloat()) + " Rs"
            remove.setOnClickListener {
                var i = qty.text.toString().toFloat().round(2)
                if (i > dat.unitqty.toFloat()) {
                    i -= dat.unitqty.toFloat().round(2)
                    i = ((i * 100.0).roundToInt() / 100.0).toFloat()
                    SharedPrefsUtil.getInstance(this@CartActivity).addToCart(dat.id, i)
                    qty.text = String.format("%.2f", i)
                    txtqty1.text = String.format("%.2f", i * dat.cost.toFloat()) + " Rs"
                } else {
                    SharedPrefsUtil.getInstance(this@CartActivity).removeFromCart(aList.removeAt(position).id)
                    notifyDataSetChanged()
                }
            }
            addi.setOnClickListener {
                var i = qty.text.toString().toFloat() + dat.unitqty.toFloat().round(2)
                if (i <= dat.available_quantity) {
                    i = ((i * 100.0).roundToInt() / 100.0).toFloat()
                    SharedPrefsUtil.getInstance(this@CartActivity).addToCart(dat.id, i)
                    qty.text = String.format("%.2f", i)
                    txtqty1.text = String.format("%.2f", i * dat.cost.toFloat()) + " Rs"
                } else
                    Toast.makeText(applicationContext, "Max available qty reached", Toast.LENGTH_SHORT).show()
            }
            imageButton.setOnClickListener {
                SharedPrefsUtil.getInstance(this@CartActivity).removeFromCart(aList.removeAt(position).id)
                notifyDataSetChanged()
            }
            return view
        }

        private fun removeS(str: String?): String? {
            var str = str
            if (str != null && str.isNotEmpty() && str[str.length - 1] == 's') {
                str = str.substring(0, str.length - 1)
            }
            return str
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}