package com.prakritifresh.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.RatingBar.OnRatingBarChangeListener
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.storage.FirebaseStorage
import com.mrittica.Order
import com.mrittica.OrderItem
import com.mrittica.ShopItem
import com.mrittica.constants.OrderStatus
import com.mrittica.utils.toTitleCase
import com.prakritifresh.GlideApp
import com.prakritifresh.R
import com.prakritifresh.utils.Constants
import com.prakritifresh.utils.SharedPrefsUtil
import kotlinx.android.synthetic.main.activity_order_details.*
import kotlinx.android.synthetic.main.activity_order_details_bottom_sheet.*
import java.util.*

class OrderDetailsActivity : AppCompatActivity(), OrderStatus {

    var order: Order? = Order()
    var order_items: List<OrderItem>? = null
    private var orderDetAdapter = OrderDetAdapter()
    var db = FirebaseFirestore.getInstance()
    var listView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_details)

        rv_orders_det.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        order_items = ArrayList()
        order = intent.extras!!.getSerializable("order") as Order?
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.subtitle = order!!.order_id
        supportActionBar!!.title = "Order id:"

        order_items = order!!.det
        rv_orders_det.layoutManager = LinearLayoutManager(this)
        rv_orders_det.adapter = orderDetAdapter
        val llBottomSheet = findViewById<LinearLayout>(R.id.bottom_sheet)
        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet)
        if (order!!.status == OrderStatus.DELIVERED) {
            bottomSheetBehavior.setHideable(false)
        } else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
        }
        tv_feedback.setOnClickListener { l: View? ->
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }

        submitButton.setOnClickListener {
            order!!.isFeedbackSubmitted = true
            FirebaseFirestore.getInstance().collection("orders")
                    .document(order!!.order_id!!)
                    .set(order!!)
                    .addOnSuccessListener { x: Void? -> Toast.makeText(applicationContext, "sucess", Toast.LENGTH_SHORT).show() }
                    .addOnFailureListener { x: Exception? -> Toast.makeText(applicationContext, "Failed", Toast.LENGTH_SHORT).show() }
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            assignRatingList()
        }
        listView = findViewById(R.id.rating_list)
        assignRatingList()
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onStateChanged(view: View, i: Int) {
                when (i) {
                    BottomSheetBehavior.STATE_COLLAPSED -> tv_feedback.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up_white_24dp, 0)
                    BottomSheetBehavior.STATE_EXPANDED -> tv_feedback.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_drop_up_white_24dp, 0)
                    else -> {
                    }
                }
            }

            override fun onSlide(view: View, v: Float) {}
        })

        ib_phone.setOnClickListener { startActivity(Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + "+919800073028"))) }
        ib_whatsapp.setOnClickListener { startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=919800073028&text=order_id:" + order!!.order_id))) }
    }

    private fun assignRatingList() {
        if (order!!.isFeedbackSubmitted) {
            submitButton.visibility = View.GONE
        }
        listView!!.adapter = object : ArrayAdapter<OrderItem?>(this, R.layout.row_product_rating, order_items!!) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                var v = convertView
                if (v == null) {
                    val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    v = inflater.inflate(R.layout.row_product_rating, null)
                }
                val i = order_items!![position]
                val tt = v!!.findViewById<TextView>(R.id.tv_product)
                tt.text = i.name
                val r = v.findViewById<RatingBar>(R.id.product_RatingBar)
                r.rating = i.rating
                if (order!!.isFeedbackSubmitted) {
                    // r.setEnabled(false);
                    r.setIsIndicator(true)
                }
                r.onRatingBarChangeListener = OnRatingBarChangeListener { _: RatingBar?, rating: Float, _: Boolean -> order_items!![position].rating = rating }
                return v
            }
        }
    }

    internal inner class OrderDetAdapter : RecyclerView.Adapter<OrderDetAdapter.OrderDetHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDetHolder {
            val view = LayoutInflater.from(applicationContext).inflate(R.layout.row_order_details, parent, false)
            return OrderDetHolder(view)
        }

        override fun onBindViewHolder(holder: OrderDetHolder, position: Int) {
            holder.tv_name.text = order_items!![position].name.toTitleCase() + " - " + order_items!![position].subcat.toTitleCase()
            holder.tv_name.isSelected = true
            holder.tv_price.text = "₹" + order_items!![position].cost + "/" + order_items!![position].unittype
            holder.tv_qty.text = order_items!![position].number.toString() + " " + order_items!![position].unittype.trim { it <= ' ' }
            holder.tv_total.text = "₹ " + order_items!![position].number * order_items!![position].cost.toFloat()
            val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/" + order_items!![position].itemID + ".jpg")
            GlideApp.with(this@OrderDetailsActivity)
                    .load(storageReference)
                    .error(R.mipmap.prakritifresh_logo)
                    .into(holder.imageView)
            holder.tv_tp.visibility = View.INVISIBLE
            holder.cart.visibility = View.INVISIBLE
            db.collection("shopitems")
                    .whereEqualTo("shop", order_items!![position].shop)
                    .whereEqualTo("itemID", order_items!![position].itemID)
                    .get()
                    .addOnSuccessListener { queryDocumentSnapshots: QuerySnapshot ->
                        if (queryDocumentSnapshots.documents.size > 0) {
                            holder.cart.visibility = View.VISIBLE
                            holder.tv_tp.visibility = View.VISIBLE
                            val shopItem = queryDocumentSnapshots.documents[0].toObject(ShopItem::class.java)
                            holder.tv_tp.text = Html.fromHtml("Today's Rate: <b>" + shopItem!!.cost + "/" + shopItem.unittype + "</b>")
                        }
                    }
            if (SharedPrefsUtil.getInstance(this@OrderDetailsActivity).isInCart(order_items!![position].id)) {
                holder.cart.setImageResource(R.drawable.ic_cart)
            } else {
                holder.cart.setImageResource(R.drawable.ic_cart_empty)
            }
            holder.cart.setOnClickListener { v: View? ->
                if (SharedPrefsUtil.getInstance(this@OrderDetailsActivity).isInCart(order_items!![position].id)) {
                    SharedPrefsUtil.getInstance(this@OrderDetailsActivity).removeFromCart(order_items!![position].id)
                    holder.cart.setImageResource(R.drawable.ic_cart_empty)
                } else {
                    SharedPrefsUtil.getInstance(this@OrderDetailsActivity).addToCart(order_items!![position].id, java.lang.Float.valueOf(order_items!![position].unitqty))
                    holder.cart.setImageResource(R.drawable.ic_cart)
                }
            }

            val time = System.currentTimeMillis() - order!!.orderedTime;

            if (!order?.status.equals(OrderStatus.DELIVERED)) {
                holder.btnReplace.visibility = View.GONE
            } else
                holder.btnReplace.visibility = when (order_items!![position].category) {
                    Constants.VEGETABLES, Constants.FRUITS -> {
                        if (time < 2 * 24 * 60 * 60 * 1000)
                            View.VISIBLE
                        else
                            View.GONE
                    }
                    Constants.STAPLES -> {
                        if (time < 7 * 24 * 60 * 60 * 1000)
                            View.VISIBLE
                        else
                            View.GONE
                    }
                    else -> View.GONE
                }

            holder.btnReplace.setOnClickListener {
                startActivity(Intent(this@OrderDetailsActivity, ReplaceRequestActivity::class.java).putExtra("key", order?.order_id + ":::" + order_items?.get(position)?.itemID))
            }

        }

        override fun getItemCount() = order_items!!.size

        internal inner class OrderDetHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imageView: ImageView = itemView.findViewById(R.id.iv_item)
            var cart: ImageView = itemView.findViewById(R.id.cart)
            var tv_name: TextView = itemView.findViewById(R.id.tv_order_name)
            var tv_qty: TextView = itemView.findViewById(R.id.tv_order_qty)
            var tv_price: TextView = itemView.findViewById(R.id.tv_order_p)
            var tv_tp: TextView = itemView.findViewById(R.id.tv_order_tr)
            var tv_total: TextView = itemView.findViewById(R.id.tv_order_total)
            var btnReplace: Button = itemView.findViewById(R.id.btn_replace)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}