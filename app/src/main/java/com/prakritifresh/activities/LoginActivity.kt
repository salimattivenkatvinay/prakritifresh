package com.prakritifresh.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.AuthUI.IdpConfig.EmailBuilder
import com.firebase.ui.auth.AuthUI.IdpConfig.PhoneBuilder
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.prakritifresh.R
import com.prakritifresh.utils.Constants
import com.prakritifresh.viewModels.SubscriptionViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : Activity() {
    var progressDialog: ProgressDialog? = null
    var db = FirebaseFirestore.getInstance()

    private fun updateUI(user: FirebaseUser?) {
        startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin.setOnClickListener {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(
                                    listOf(
                                            PhoneBuilder().setDefaultCountryIso("IN").build(),
                                            EmailBuilder().build())
                            )
                            .setTheme(R.style.LoginTheme)
                            .setLogo(R.mipmap.prakritifresh_logo)
                            .setIsSmartLockEnabled(false)
                            .build(),
                    RC_SIGN_IN)
        }
        btn_signup.setOnClickListener(View.OnClickListener { v: View? -> btnLogin.performClick() })
        progressDialog = ProgressDialog(this@LoginActivity)
        progressDialog!!.setMessage("Logging in ...")
        progressDialog!!.setCanceledOnTouchOutside(false)
        val mAuth = FirebaseAuth.getInstance()
        val currentUser = mAuth.currentUser
        if (currentUser != null) {
            //Toast.makeText(getApplicationContext(), currentUser.getUid().toString(), Toast.LENGTH_SHORT).show();
            updateUI(currentUser)
        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(
                                    listOf(
                                            PhoneBuilder().setDefaultCountryIso("IN").build() //,new AuthUI.IdpConfig.EmailBuilder().build()
                                    )
                            )
                            .setTosAndPrivacyPolicyUrls("", "https://docs.google.com/document/d/1JqffT3lmydPYywFBnj8Dr1qdhhITTZFptKOfn_ELfzI/edit")
                            .setTheme(R.style.LoginTheme)
                            .setLogo(R.mipmap.prakritifresh_logo)
                            .setIsSmartLockEnabled(false)
                            .build(),
                    RC_SIGN_IN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == RESULT_OK) {
                btnLogin!!.visibility = View.GONE
                FirebaseInstanceId.getInstance().instanceId
                        .addOnCompleteListener { task: Task<InstanceIdResult> ->
                            if (!task.isSuccessful) {
                                Log.w(Constants.TAG, "getInstanceId failed", task.exception)
                                return@addOnCompleteListener
                            }
                            val token = task.result.token
                            Toast.makeText(applicationContext, token, Toast.LENGTH_SHORT).show()
                            db.collection("Users").document(FirebaseAuth.getInstance().uid!!)
                                    .set(mapOf("token" to token), SetOptions.merge())
                        }
                SubscriptionViewModel.setIsCached()
                startActivity(Intent(this, SignupActivity::class.java))
                finish()
            } else {
                if (response == null) {
                    showSnackbar(R.string.sign_in_cancelled)
                } else if (response.error!!.errorCode == ErrorCodes.NO_NETWORK) {
                    showSnackbar(R.string.no_internet_connection)
                } else {
                    showSnackbar(R.string.unknown_error)
                    Log.e(Constants.TAG, "Sign-in error: ", response.error)
                }
                finish()
            }
        } else finish()
    }

    private fun showSnackbar(error: Any) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        startActivity(Intent(applicationContext, HomeActivity::class.java))
        finishAffinity()
    }

    companion object {
        private const val RC_SIGN_IN = 213
    }
}