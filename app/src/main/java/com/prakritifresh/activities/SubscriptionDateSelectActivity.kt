package com.prakritifresh.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mrittica.OrderItem
import com.prakritifresh.R
import com.prakritifresh.fragments.SubscriptionDatesSelectFragment
import com.prakritifresh.fragments.SubscriptionMonthFragment
import com.prakritifresh.viewModels.SubscriptionViewModel

interface SubscriptionListener {
    fun onSubscribe()
}

class SubscriptionDateSelectActivity : AppCompatActivity(), SubscriptionListener {

    lateinit var shopItem: OrderItem
    lateinit var subkey: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_date_select)

        shopItem = intent.extras?.getSerializable("item") as OrderItem
        subkey = intent.extras?.getString("subkey", "")!!

        onSubscribe()
    }

    override fun onSubscribe() {
        SubscriptionViewModel.getSubscriptionDetails(subkey) { subscription ->
            if (subscription?.itemsSubscribed?.contains(shopItem.itemID) == true)
                supportFragmentManager.beginTransaction().replace(R.id.fragment, SubscriptionMonthFragment(shopItem, subscription)).commit()
            else
                supportFragmentManager.beginTransaction().replace(R.id.fragment, SubscriptionDatesSelectFragment(this, shopItem, subscription!!)).commit()
        }
    }
}