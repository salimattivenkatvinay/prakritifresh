package com.prakritifresh.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mrittica.Order;
import com.mrittica.constants.OrderStatus;
import com.mrittica.utils.ConverterKt;
import com.prakritifresh.GlideApp;
import com.prakritifresh.R;
import com.prakritifresh.fragments.AccountActivity;
import com.prakritifresh.fragments.FavouriteFragment;
import com.prakritifresh.fragments.HomeFragment;
import com.prakritifresh.fragments.OrdersFragment;
import com.prakritifresh.fragments.SubscriptionOrdersFragment;
import com.prakritifresh.fragments.SubscriptionsFragment;
import com.prakritifresh.listners.UpdateListener;
import com.prakritifresh.utils.Constants;
import com.prakritifresh.utils.SharedPrefsUtil;
import com.prakritifresh.viewModels.ShopItemsViewModel;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener, UpdateListener {
    public static FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean onc = false;

    private TextView tvNoOfCartItems;
    private TextView tv_location;
    private TextView tv_title;

    private StorageReference profileref = FirebaseStorage.getInstance().getReference().child("profile_image/" + FirebaseAuth.getInstance().getUid() + ".jpg");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        FirebaseMessaging.getInstance().subscribeToTopic(Constants.topic);

        tvNoOfCartItems = findViewById(R.id.textOne);
        tv_location = findViewById(R.id.tv_location);
        tv_title = findViewById(R.id.tv_title);

        tv_location.setText(SharedPrefsUtil.getInstance(this).getRegion());
        tv_location.setOnClickListener(this);

        ImageView profile_image = findViewById(R.id.iv_profile);

        GlideApp.with(this)
                .load(profileref)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .error(R.drawable.ic_account_solid)
                .circleCrop()
                .into(profile_image);

        profile_image.setOnClickListener(this);

        findViewById(R.id.ic_qr_scan).setOnClickListener(this);
        findViewById(R.id.cart_item).setOnClickListener(this);
        findViewById(R.id.iv_get_location).setOnClickListener(this);

        BottomNavigationView navView = findViewById(R.id.nav_view);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new HomeFragment()).commit();

        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
                = item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    hideLocationSelector(View.VISIBLE, View.GONE, "");
                    mFragmentManager.beginTransaction().replace(R.id.containerView, new HomeFragment()).commit();
                    return true;
                case R.id.navigation_subscription:
                    hideLocationSelector(View.GONE, View.VISIBLE, "Subscriptions");
//                    if (!SharedPrefsUtil.getInstance(this).isPlusUser())
                        mFragmentManager.beginTransaction().replace(R.id.containerView, new SubscriptionsFragment()).commit();
  /*                  else
                        mFragmentManager.beginTransaction().replace(R.id.containerView, new SubscriptionOrdersFragment()).commit();*/
                    return true;
                case R.id.navigation_fav:
                    hideLocationSelector(View.GONE, View.VISIBLE, "Favourites");
                    mFragmentManager.beginTransaction().replace(R.id.containerView, new FavouriteFragment()).commit();
                    return true;
                case R.id.navigation_order:
                    hideLocationSelector(View.GONE, View.VISIBLE, "My Orders");
                    mFragmentManager.beginTransaction().replace(R.id.containerView, new OrdersFragment()).commit();
                    return true;
            }
            return false;
        };
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (null != FirebaseAuth.getInstance().getCurrentUser())
            FirebaseFirestore.getInstance().collection("orders")
                    .whereEqualTo("user_id", FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .whereEqualTo("status", OrderStatus.DELIVERED)
                    .orderBy("orderedTime", Query.Direction.DESCENDING)
                    .limit(1L)
                    .get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful())
                            for (DocumentSnapshot documentSnapshot : task.getResult()) {
                                Order m = documentSnapshot.toObject(Order.class);
                                m.setOrder_id(documentSnapshot.getId());
                                if (m.getRating() == 0f) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                                    builder.setTitle("Rate your last order");
                                    final View customLayout = getLayoutInflater().inflate(R.layout.custom_layout, null);
                                    builder.setView(customLayout);

                                    // add a button
                                    builder.setPositiveButton("submit", (dialog, which) -> {
                                        RatingBar editText = customLayout.findViewById(R.id.ratingBar);
                                        if (editText.getRating() == 0f) {
                                            Toast.makeText(getApplicationContext(), "pls select rating > 0", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Map<String, Object> updates = new HashMap<>();
                                            updates.put("rating", editText.getRating());
                                            FirebaseFirestore.getInstance().collection("orders").document(m.getOrder_id()).update(updates);
                                            dialog.dismiss();
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }
                            }
                        else Log.i(Constants.TAG, task.getException().getMessage());
                    });
    }

    private void hideLocationSelector(int tv_locationVisibility, int tv_titleVisibility, String title) {
        tv_location.setVisibility(tv_locationVisibility);
        findViewById(R.id.iv_get_location).setVisibility(tv_locationVisibility);
        tv_title.setVisibility(tv_titleVisibility);
        tv_title.setText(title);

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_get_location:
            case R.id.tv_location:
                fetchLocations();
                break;
            case R.id.cart_item:
                startActivity(new Intent(this, CartActivity.class));
                break;
            case R.id.ic_qr_scan:
                startActivity(new Intent(this, ScannedBarcodeActivity.class));
                break;
            case R.id.iv_profile:
                if (null != FirebaseAuth.getInstance().getCurrentUser()) {
                    startActivity(new Intent(this, AccountActivity.class));
                    Animatoo.animateSlideRight(this);
                } else
                    startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }

    private void update() {
        if (tvNoOfCartItems == null) return;
        tvNoOfCartItems.setText(String.format(Locale.ENGLISH, "%d", SharedPrefsUtil.getInstance(this).getCartSize()));
    }

    @Override
    public void cartUpdate() {
        update();
    }

    @Override
    protected void onResume() {
        super.onResume();
        update();
        if (SharedPrefsUtil.getInstance(this).getRegion().equals("")) fetchLocations();
    }

    private void fetchLocations() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Fetching locations");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        FirebaseFirestore.getInstance().collection("central_data")
                .document("regions")
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    progressDialog.hide();
                    Map<String, Object> map = documentSnapshot.getData();
                    HashMap<String, String> myNewHashMap = new HashMap<>();
                    Log.i(Constants.TAG, "fetchLocations:" +  map);
                    for (String key : map.keySet()) {
//                        if (key.toString().equals("000000")) continue;
                        myNewHashMap.put(ConverterKt.toTitleCase(map.get(key).toString()), key);
                    }
                    getLocation(myNewHashMap);
                }).addOnFailureListener(e -> {
                    progressDialog.hide();
                    Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
        );
    }

    @SuppressLint("RestrictedApi")
    private void getLocation(Map<String, String> locations) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Location");
        String[] regions = locations.keySet().toArray(new String[0]);

        builder.setItems(regions, (dialog, which) -> {
            String reg = regions[which];
            String loc = locations.get(reg);
            SharedPrefsUtil.getInstance(HomeActivity.this).changeLocation(loc, reg);
            tv_location.setText(reg);
            update();
            ShopItemsViewModel.INSTANCE.resetCache();
            mFragmentManager.beginTransaction().replace(R.id.containerView, new HomeFragment()).commit();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
