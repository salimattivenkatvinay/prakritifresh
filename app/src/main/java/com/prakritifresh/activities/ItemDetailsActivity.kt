package com.prakritifresh.activities

import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.firebase.storage.FirebaseStorage
import com.mrittica.ShopItem
import com.mrittica.utils.round
import com.mrittica.utils.toTitleCase
import com.prakritifresh.GlideApp
import com.prakritifresh.R
import com.prakritifresh.utils.SharedPrefsUtil
import kotlinx.android.synthetic.main.activity_item_details.*
import kotlin.math.roundToInt

class ItemDetailsActivity : AppCompatActivity() {

    private lateinit var prod: ShopItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_details)

        prod = intent.getSerializableExtra("item") as ShopItem
        prod.name = prod.name.toTitleCase()
        prod.subcat = prod.subcat.toTitleCase()

        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (prod.subcat == "Main") {
            supportActionBar?.title = prod.name
            vegname1.text = Html.fromHtml(prod.name + "<br>")
        } else {
            supportActionBar?.title = prod.subcat
            supportActionBar?.subtitle = prod.name
            vegname1.text = Html.fromHtml(prod.subcat + "<br><small><i>" + prod.name + "</i></small>")
        }

        prc1.text = "₹" + prod.cost.toFloat().roundToInt() + "/ " + prod.unittype.trim().trimEnd { it == 's' }
        val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/" + prod.itemID + ".jpg")

        GlideApp.with(applicationContext)
                .load(storageReference)
                .thumbnail(0.1f)
                .error(R.mipmap.prakritifresh_logo)
                .into(imageView)

        if (prod.available == "no" || prod.unitqty.toFloat() > prod.available_quantity) {
            imout1.visibility = View.VISIBLE
            ll_cart.visibility = View.INVISIBLE
        } else {
            imout1.visibility = View.INVISIBLE
            ll_cart.visibility = View.VISIBLE
        }

        available_qty.text = "Available : ${prod.available_quantity.round(2)}"


        if (SharedPrefsUtil.getInstance(this).isInCart(prod.id)) {
            cart.visibility = View.GONE
            ll_cart_qty.visibility = View.VISIBLE
            nfunit.text = String.format("%.2f", SharedPrefsUtil.getInstance(this).getCartItemQty(prod.id))
            txtqt1.text = "₹" + String.format("%.2f", SharedPrefsUtil.getInstance(this).getCartItemQty(prod.id) * prod.cost.toFloat())
        } else {
            cart.visibility = View.VISIBLE
            ll_cart_qty.visibility = View.GONE
        }
        cart.text = "Add to cart"
        cart.background = resources.getDrawable(R.drawable.add_to_cart_bg)
        cart.setTextColor(resources.getColor(R.color.white))

        removeit.setOnClickListener {
            var i: Float = nfunit.text.toString().toFloat().round(2)
            if (i > prod.unitqty.toFloat()) {
                i -= prod.unitqty.toFloat().round(2)
                i = (i * 100).roundToInt() / 100f
                SharedPrefsUtil.getInstance(this).addToCart(prod.id, i)
                txtqt1.text = "₹" + String.format("%.2f", i * prod.cost.toFloat())
                nfunit.text = String.format("%.2f", i)
            } else {
                SharedPrefsUtil.getInstance(this).removeFromCart(prod.id)
                cart.visibility = View.VISIBLE
                ll_cart_qty.visibility = View.GONE
            }
        }
        addit.setOnClickListener {
            var i: Float = nfunit.text.toString().toFloat() + prod.unitqty.toFloat().round(2)
            if (i <= prod.available_quantity) {
                i = (i * 100).roundToInt() / 100f
                SharedPrefsUtil.getInstance(this).addToCart(prod.id, i)
                nfunit.text = String.format("%.2f", i)
                txtqt1.text = "₹" + String.format("%.2f", i * prod.cost.toFloat())
            } else
                Toast.makeText(applicationContext, "Max available qty reached", Toast.LENGTH_SHORT).show()
        }

        cart.setOnClickListener {
            SharedPrefsUtil.getInstance(this).addToCart(prod.id, prod.unitqty.toFloat())
            cart.visibility = View.GONE
            ll_cart_qty.visibility = View.VISIBLE
            nfunit.text = String.format("%.2f", SharedPrefsUtil.getInstance(this).getCartItemQty(prod.id))
            txtqt1.text = "₹" + String.format("%.2f", SharedPrefsUtil.getInstance(this).getCartItemQty(prod.id) * prod.cost.toFloat())
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Animatoo.animateSlideDown(this)  //fire the zoom animation
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
