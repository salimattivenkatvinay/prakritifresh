package com.prakritifresh.activities

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.prakritifresh.R
import kotlinx.android.synthetic.main.activity_links.*


class LinksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_links)

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "About"

        lv_links.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                listOf("About Us", "Terms and Conditions", "Privacy policy", "Refund policy"))

        lv_links.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            when (position) {
                0 -> startActivity(Intent(this, TextActivity::class.java).putExtra("path", "About Us.pdf"))
                1 -> startActivity(Intent(this, TextActivity::class.java).putExtra("path", "Mrittica_Terms_and_Conditions.pdf"))
                2 -> startActivity(Intent(this, TextActivity::class.java).putExtra("path", "Mrittica_Privacy_Policy.pdf"))
                else -> startActivity(Intent(this, TextActivity::class.java).putExtra("path", "Mrittica_Refund_and_Cancellation.pdf"))
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
