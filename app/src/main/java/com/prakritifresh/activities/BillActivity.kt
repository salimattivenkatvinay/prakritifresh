package com.prakritifresh.activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import com.google.gson.Gson
import com.mrittica.Order
import com.mrittica.OrderItem
import com.mrittica.constants.OrderStatus
import com.mrittica.constants.OrderStatus.PENDING
import com.mrittica.constants.OrderTypes
import com.mrittica.slot.Slot
import com.mrittica.slot.Slots
import com.mrittica.utils.convertToString
import com.mrittica.utils.roundToCurrency
import com.prakritifresh.MyApp
import com.prakritifresh.R
import com.prakritifresh.adapters.BillAdapter
import com.prakritifresh.pojos.SubscriptionF
import com.prakritifresh.utils.Constants
import com.prakritifresh.utils.Constants.*
import com.prakritifresh.utils.SharedPrefsUtil
import com.prakritifresh.viewModels.SubscriptionViewModel
import kotlinx.android.synthetic.main.bill_layout.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt


class BillActivity : AppCompatActivity() {
    internal lateinit var adapter: BillAdapter
    private var finAmount = 0.0f
    private var totAmount = 0.0f
    internal var data = ArrayList<OrderItem>()
    private var deli = 20f
    internal var db = FirebaseFirestore.getInstance()
    private var scheduledTime = 0L
    private var lat: Double = 0.0
    private var lon: Double = 0.0
    private var landmark = ""
    private val GET_ADDRESS = 2
    private var type = ""
    private lateinit var functions: FirebaseFunctions
    private var coupon: String? = null
    var slots: Slots? = null

    var userDetFetched = false
    var slotDetFetched = false

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bill_layout)

        functions = FirebaseFunctions.getInstance()
        deliAddress.isEnabled = false
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Bill"
        tv_scheduled_for.text = "Scheduled for " + (System.currentTimeMillis() + 3.6e+6.toLong()).convertToString("EEE, MMM d, hh:mm aaa")
        data = intent.getSerializableExtra(CART_LIST) as ArrayList<OrderItem>
        type = intent.extras!!.getString(orderType, oneTime)

        if (type != OrderTypes.ONETIME.name) {
            ll_coupon.visibility = View.GONE
        }
        checkoutbill.setOnClickListener { updateUserAddress() }
        btn_ea.setOnClickListener { startActivityForResult(Intent(applicationContext, MapsActivity::class.java), GET_ADDRESS) }

        adapter = BillAdapter(this, data)
        totAmount = data.map { (it.number * it.cost.toFloat()).roundToCurrency() }.sum()
        finAmount = totAmount
        listview_bill.adapter = adapter
        if (data.size != 0) {
            totitemsbi.visibility = View.VISIBLE
            setAmount()
            //checkoutbill.visibility = View.VISIBLE
            listview_bill.visibility = View.VISIBLE
        } else {
            checkoutbill.visibility = View.INVISIBLE
            listview_bill.visibility = View.INVISIBLE
            Toast.makeText(applicationContext, "No items", Toast.LENGTH_SHORT).show()
        }
        setListViewHeightBasedOnChildren(listview_bill)
        getUserAddress()
    }

    private fun setAmount() {
        if (finAmount >= 99.0f || type != OrderTypes.ONETIME.name) {
            upm.text = "₹ " + String.format("%.2f", (finAmount))
            tvProdsCost.text = "₹ " + String.format("%.2f", (totAmount))
            tvDeliCharge.text = "₹ 0.00";
            tvTotAmount.text = "₹ " + String.format("%.2f", (finAmount))
            deli = 0f
        } else {
            upm.text = "₹ " + String.format("%.0f0", (finAmount + 20f))
            tvDel.text = "Delivery charge(₹ 0 for orders above ₹ 99)"
            tvProdsCost.text = "₹ " + String.format("%.2f", (totAmount))
            tvDeliCharge.text = "₹ 20.00";
            tvTotAmount.text = "₹ " + String.format("%.2f", (finAmount + 20.0f))
            deli = 20f
        }
    }

    private fun updateUserAddress() {
        val addressNew = deliAddress.text.toString()
        val phoneNew = deliPhone.text.toString().replace("+91", "")
        if (addressNew.matches("".toRegex()) || phoneNew.matches("".toRegex())) {
            Toast.makeText(applicationContext, "Please fill in all fields", Toast.LENGTH_SHORT).show()
        } else if (lat == 0.0 || lon == 0.0) {
            Toast.makeText(applicationContext, "Map Co-ordinates missing, pls edit the existing address", Toast.LENGTH_SHORT).show()
        } else if (phoneNew.length == 10 && (phoneNew.startsWith("9") || phoneNew.startsWith("8") || phoneNew.startsWith("7") || phoneNew.startsWith("6"))) {
            placeOrder(addressNew, landmark, phoneNew)
        } else {
            Toast.makeText(applicationContext, "Please enter a valid phone number", Toast.LENGTH_SHORT).show()
        }
    }

    private fun placeOrder(address_new: String, landmark_new: String, phone_new: String) {
        val progressDialog = ProgressDialog(this@BillActivity)
        progressDialog.setMessage("Placing order....")
        progressDialog.show()
        val cal = Calendar.getInstance()
        cal.time = Date()
//            cal.add(Calendar.HOUR_OF_DAY, 1) // adds one hour
        var isInDeliverableSlot = false
        loop@ for (slot in slots?.slot!!) {
            if (cal.get(Calendar.HOUR_OF_DAY) >= slot.min && cal.get(Calendar.HOUR_OF_DAY) < slot.max - 1) {
                isInDeliverableSlot = true
                break@loop
            }
        }
        if (isInDeliverableSlot || type != OrderTypes.ONETIME.name) {
            val now = System.currentTimeMillis()
//            val orders: Map<String, List<ShopItem>> = data.groupBy { it.shop }
            val order = Order()
            order.user_id = FirebaseAuth.getInstance().uid
            order.userName = FirebaseAuth.getInstance().currentUser?.displayName
            order.det = data
            order.address = address_new
            order.landmark = landmark_new
            order.phno = phone_new
            order.orderedTime = now
            scheduledTime = cal.timeInMillis
            order.scheduledTime = scheduledTime
            order.status = if (type != OrderTypes.ONETIME.name) OrderStatus.PACKED else PENDING
            order.amount = finAmount
            order.shop_id = data[0].shop
            order.lat = lat
            order.lng = lon
            order.delivery = deli
            order.coupon = coupon
            order.discount = totAmount - finAmount
            order.orderType = type


            db.collection("orders")
                    .add(order)
                    .addOnCompleteListener { task ->
                        progressDialog.dismiss()
                        if (task.isSuccessful) {
                            if (coupon != null)
                                MyApp.db.collection("coupons").document(coupon.toString())
                                        .update("users." + FirebaseAuth.getInstance().currentUser?.phoneNumber, task.result?.id)
                            when (OrderTypes.valueOf(type)) {
                                OrderTypes.PLUS -> {
                                    val key = System.currentTimeMillis().convertToString(dateFormat)
                                    val sub = SubscriptionF()
                                    sub.address = address
                                    sub.endDate = addDays(Date(), 30)
                                    sub.landmark = landmark
                                    sub.lat = lat
                                    sub.lon = lon
                                    sub.paidAmount = 0f
                                    sub.phno = phone_new
                                    sub.shopId = data[0].shop
                                    sub.pincode = SharedPrefsUtil.getInstance(this).pincode
                                    sub.startDate = addDays(Date(), 1)
                                    sub.userId = FirebaseAuth.getInstance().uid
                                    MyApp.db.collection(subscriptions).document(task.result?.id
                                            ?: "")
                                            .set(sub)
                                            .addOnSuccessListener {
                                                SubscriptionViewModel.setIsCached()
                                            }
                                }
                                OrderTypes.SUBSCRIPTION_PAYMENT -> {
                                    db.collection(subscriptions).document(data[0].name!!)
                                            .update(mapOf(
                                                    "paidAmount" to FieldValue.increment(data[0].cost.toDouble())
                                            ))
                                            .addOnSuccessListener {
                                                SubscriptionViewModel.setIsCached()
                                            }
/*
                                    val sfDocRef = db.collection(Constants.subscriptions).document(data[0].name!!);
                                    db.runTransaction { transaction ->
                                        val snapshot = transaction.get(sfDocRef)

                                        // Note: this could be done without a transaction
                                        //       by updating the population using FieldValue.increment()
                                        val newPopulation = snapshot.getDouble("paidAmount")!! + 1
                                        transaction.update(sfDocRef, "paidAmount", newPopulation)

                                        // Success
                                        null
                                    }.addOnSuccessListener { Log.d(TAG, "Transaction success!") }
                                            .addOnFailureListener { e -> Log.w(TAG, "Transaction failure.", e) }*/
                                }
                                else ->
                                    SharedPrefsUtil.getInstance(this).clearCart()
                            }
                            Toast.makeText(applicationContext, "success", Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this, ThankYouActivity::class.java)
                                    .putExtra(Constants.scheduledTime, scheduledTime)
                                    .putExtra("orderId", task.result!!.id)
                                    .putExtra("amount", finAmount.toString())
                                    .putExtra("shopID", data[0].shop)
                                    .putExtra("orderType", type)
                                    .putExtra("type", "orders"))
                            finishAffinity()
                        } else {
                            Log.e(TAG, "placeOrder: " + task.exception!!.message)
                            Toast.makeText(applicationContext, "order failed. retry!!", Toast.LENGTH_SHORT).show()
                        }
                    }
        } else {
            AlertDialog.Builder(this@BillActivity)
                    .setTitle(appName)
                    .setMessage("Sorry!. Out of delivery timings\n[${slots!!.slot?.map { slot: Slot -> "${slot.min} - ${slot.max}" }}]. try again later")
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        startActivity(Intent(this@BillActivity, HomeActivity::class.java))
                        finishAffinity()
                    }
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
            progressDialog.hide()
        }
    }

    private fun getUserAddress() {
        deliPhone.setText(FirebaseAuth.getInstance().currentUser!!.phoneNumber.orEmpty())
        val loading = ProgressDialog(this@BillActivity)
        loading.setMessage(Html.fromHtml("<b><h2>Retrieving user details...."))
        loading.show()
        //loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false)
        FirebaseFirestore.getInstance().collection("Users")
                .document(FirebaseAuth.getInstance().currentUser!!.uid)
                .get()
                .addOnCompleteListener { task ->
                    loading.hide()
                    getSlots()
                    deliAddress.text = ""
                    if (task.isSuccessful) {
                        if (task.result!!.exists()) {
                            userDetFetched = true
                            if (slotDetFetched)
                                checkoutbill.visibility = View.VISIBLE

                            val jsonObject = task.result!!.data

                            if (jsonObject?.containsKey("landmark")!!) {
                                landmark = jsonObject["landmark"].toString()
                                deliAddress.append("Near " + jsonObject["landmark"]!!.toString() + "\n")
                            }
                            if (jsonObject.containsKey("plot")) {
                                deliAddress.append(jsonObject["plot"]!!.toString() + ",\n ")
                            }
                            if (jsonObject.containsKey("street")) {
                                deliAddress.append(jsonObject["street"]!!.toString() + ",\n ")
                            }
                            if (jsonObject.containsKey("city")) {
                                deliAddress.append(jsonObject["city"]!!.toString() + ",\n ")
                            }
                            if (jsonObject.containsKey("state")) {
                                deliAddress.append(jsonObject["state"]!!.toString() + ".\n ")
                            }
                            if (jsonObject.containsKey("pin")) {
                                deliAddress.append("PIN - " + jsonObject["pin"]!!.toString())
                            }
                            if (jsonObject.containsKey("lat")) {
                                lat = jsonObject["lat"]!!.toString().toDouble()
                            }
                            if (jsonObject.containsKey("lon")) {
                                lon = jsonObject["lon"]!!.toString().toDouble()
                            }
                        } else {
                            Toast.makeText(applicationContext, task.exception!!.localizedMessage, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setListViewHeightBasedOnChildren(listView: ListView) {
        val listAdapter = listView.adapter
        if (listAdapter != null) {
            var totalHeight = 0
            var view: View?
            for (i in 0 until listAdapter.count) {
                view = listAdapter.getView(i, null, listView)
                view!!.measure(0, 0)
                totalHeight += view.measuredHeight
            }
            val params = listView.layoutParams
            val dpValue = totalHeight + listView.dividerHeight * (listAdapter.count - 1)
            params.height = dpValue
            //params.height = totalHeight
            listView.layoutParams = params
            listView.requestLayout()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GET_ADDRESS) {
            getUserAddress()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun applyCoupon(v: View) {
        MyApp.db.collection("coupons").document(etCoupon.text.toString())
                .get()
                .addOnCompleteListener { task: Task<DocumentSnapshot> ->
                    if (task.isSuccessful && task.result!!.exists()) {
                        val m = task.result!!.data!!
                        coupon = task.result!!.id
                        if (m["type"] == "percentage") {
                            if (m["isSplit"].toString().toBoolean()) {
                                var discount = 0.0f
                                for (item in data) {
                                    if (item.category == VEGETABLES)
                                        discount += item.number * item.cost.toFloat() * ((m["vegPercentage"].toString().toFloat()) / 100f)
                                    else if (item.category == FRUITS)
                                        discount += item.number * item.cost.toFloat() * ((m["fruitPercentage"].toString().toFloat()) / 100f)
                                    else if (item.category == STAPLES)
                                        discount += item.number * item.cost.toFloat() * ((m["grocPercentage"].toString().toFloat()) / 100f)
                                }
                                if (discount.roundToCurrency() > m["maxDiscount"].toString().toFloat())
                                    discount = m["maxDiscount"].toString().toFloat()
                                discount = discount.roundToCurrency()
                                finAmount -= discount
                                trDiscount.visibility = View.VISIBLE
                                tvDiscount.text = "-₹ " + discount.toString()
                                setAmount()

                                etCoupon.visibility = View.GONE
                                v.visibility = View.GONE
                                couponSuccess.visibility = View.VISIBLE
                                Toast.makeText(applicationContext, "Coupon applied with discunt ${discount}", Toast.LENGTH_LONG).show()
                            } else {
                                val users = m["users"] as Map<String, String>
                                if (users.containsKey(FirebaseAuth.getInstance().currentUser?.phoneNumber)) {
                                    Toast.makeText(applicationContext, "Coupon already used", Toast.LENGTH_LONG).show()
                                } else if (users.size > m["maxUsers"].toString().toInt()) {
                                    Toast.makeText(applicationContext, "Coupon expired", Toast.LENGTH_LONG).show()
                                } else {
                                    var discount = totAmount * ((m["percentage"].toString().toFloat()) / 100)
                                    if (discount.roundToInt() > m["maxDiscount"].toString().toInt())
                                        discount = m["maxDiscount"].toString().toInt() + 0.0f

                                    finAmount -= discount.roundToInt()
                                    trDiscount.visibility = View.VISIBLE
                                    tvDiscount.text = "-₹ " + discount.roundToInt().toString()
                                    setAmount()
                                    etCoupon.visibility = View.GONE
                                    v.visibility = View.GONE
                                    couponSuccess.visibility = View.VISIBLE
                                    Toast.makeText(applicationContext, "Coupon applied with discunt ${discount.roundToInt()}", Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                    } else Toast.makeText(applicationContext, "Coupon expired", Toast.LENGTH_LONG).show()
                }
    }

    fun getCouponDiscount(v: View) {
        val map = mutableMapOf<String, String>(
                "order" to Gson().toJson(data),
                "coupon" to etCoupon.text.toString(),
                "phoneNumber" to FirebaseAuth.getInstance().currentUser?.phoneNumber.toString(),
                "totAmount" to totAmount.toString()
        )
//        Log.i(TAG, Gson().toJson(map))

//        val url = " https://us-central1-prakrithi-e1b6c.cloudfunctions.net/getCouponDiscount"
        val url = "https://api.mrittica.com/coupons/getCouponDiscount"
        val queue = Volley.newRequestQueue(this)
        val stringRequest = object : StringRequest(Method.POST, url,
                Response.Listener { response ->
                    Log.i(TAG, response)
                    val m = Gson().fromJson(response.toString(), Map::class.java)

                    if (m["msg"] == "Coupon expired") {
                        Toast.makeText(applicationContext, m["msg"].toString(), Toast.LENGTH_SHORT).show()
                        return@Listener
                    }

                    val discount = m["discount"].toString().toDouble();

                    finAmount -= discount.roundToInt()
                    trDiscount.visibility = View.VISIBLE
                    tvDiscount.text = "-₹ " + discount.roundToInt().toString()
                    setAmount()
                    etCoupon.visibility = View.GONE
                    v.visibility = View.GONE
                    couponSuccess.visibility = View.VISIBLE
                    Toast.makeText(applicationContext, "Coupon applied with discunt ${discount.roundToInt()}", Toast.LENGTH_LONG).show()
                },
                Response.ErrorListener { error ->
                    Log.e(TAG, error.toString())
                    Toast.makeText(applicationContext, error.toString(), Toast.LENGTH_SHORT).show()
                }) {
            override fun getParams(): MutableMap<String, String> {
                return map
            }
        }
        queue.add(stringRequest)
    }

    private fun getSlots() {

        val loading = ProgressDialog(this@BillActivity)
        loading.setMessage(Html.fromHtml("<b><h2>Retrieving slots...."))
        loading.show()
        //loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false)

        MyApp.db.collection("slots").document(SharedPrefsUtil.getInstance(this).pincode)
                .get()
                .addOnCompleteListener { task ->
                    loading.dismiss()
                    if (task.isSuccessful && task.result?.exists()!!) {
                        slotDetFetched = true
                        if (userDetFetched)
                            checkoutbill.visibility = View.VISIBLE
                        Log.i(TAG, task.result!!.data?.toString())
                        slots = task.result?.toObject(Slots::class.java)
                    } else {

                    }
                }
    }

    private fun addDays(dt: Date, days: Int): Date {
        val c = Calendar.getInstance()
        c.time = dt
        c.add(Calendar.DATE, days)
        c.set(Calendar.HOUR_OF_DAY, 0)
        c.set(Calendar.MINUTE, 0)
        c.set(Calendar.SECOND, 0)
        c.set(Calendar.MILLISECOND, 0)
        return c.time
    }
}