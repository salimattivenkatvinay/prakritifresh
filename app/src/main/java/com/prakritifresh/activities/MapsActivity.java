package com.prakritifresh.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.prakritifresh.R;
import com.prakritifresh.utils.Constants;
import com.prakritifresh.utils.SharedPrefsUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    private static final String TAG = "locationzzz";
    private GoogleMap mMap;
    Toolbar toolbar;
    static LatLng curLatLng = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Select location on Map");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getResources().getString(R.string.google_api_key));
        }
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                mMap.clear();
                curLatLng = place.getLatLng();
                if (curLatLng == null) return;
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title(place.getName()));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 12.0f));
            }

            @Override
            public void onError(@NonNull Status status) {
                Toast.makeText(getApplicationContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        findViewById(R.id.navigation).setOnClickListener(v -> {
            if (curLatLng == null) {
                Toast.makeText(getApplicationContext(), "no place selected", Toast.LENGTH_SHORT).show();
                return;
            }
            Log.i(Constants.TAG, "onCreate: " + curLatLng.latitude + "" + curLatLng.longitude);
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            String currentPincode = SharedPrefsUtil.getInstance(this).getPincode();

            try {
                addresses = geocoder.getFromLocation(curLatLng.latitude, curLatLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                boolean flag = false;
                Address address = null;
                for (Address a : addresses) {
                    //String address = a.getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = a.getLocality();
                    String state = a.getAdminArea();
                    String country = a.getCountryName();
                    String postalCode = a.getPostalCode();
                    Log.i(Constants.TAG, "onCreate: " + postalCode);
                    String knownName = a.getFeatureName(); // Only if available else return NULL
                    address = a;
                    if (currentPincode.equals(postalCode) || currentPincode.equals("000000")) {
                        flag = true;
                        break;
                    }
                }
                if (flag) {

                } else {
                    Toast.makeText(getApplicationContext(), "location is out of selected pincode area", Toast.LENGTH_SHORT).show();
                    address = addresses.get(0);
                }
                showEditAddressDialog(curLatLng.latitude, curLatLng.longitude, address);
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });

        findViewById(R.id.ib_dummy).setOnClickListener(v -> findViewById(R.id.navigation).performClick());
        displayLocationSettingsRequest(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();

        mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
        mMap.setOnMyLocationClickListener(onMyLocationClickListener);
        enableMyLocationIfPermitted();

        mMap.getUiSettings().setZoomControlsEnabled(true);
//        mMap.setMinZoomPreference(11);

        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(22, 88), 50.0f));

        mMap.setOnMapClickListener(latLng -> {
            mMap.clear();
            curLatLng = latLng;
            mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        });

        mMap.setOnMarkerClickListener(marker -> {
            Toast.makeText(getApplicationContext(), "Marker Selected", Toast.LENGTH_SHORT).show();
            return true;
        });

    }

    @Override
    public void onLocationChanged(Location location) {
        //mMap.clear();
//        curLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        // mMap.addMarker(new MarkerOptions().position(curLatLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 10F));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(getApplicationContext(), provider + "status changed to:" + status, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MapsActivity.this, "PERMISSION_DENIED", Toast.LENGTH_SHORT).show();
                showDefaultLocation();
            } else {
                Toast.makeText(MapsActivity.this, "PERMISSION_GRANTED", Toast.LENGTH_SHORT).show();
                enableMyLocationIfPermitted();
            }
        }
    }


    private void enableMyLocationIfPermitted() {
        if (ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission
                        (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, 1);
            }
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            mMap.clear();
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null)
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location == null)
                location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (location == null) {
                Toast.makeText(getApplicationContext(), "no prev location found", Toast.LENGTH_LONG).show();
                return;
            }
            Toast.makeText(getApplicationContext(), location.getProvider(), Toast.LENGTH_LONG).show();
            curLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.addMarker(new MarkerOptions().position(curLatLng).title("My location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(curLatLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(curLatLng, 15.0f));
            findViewById(R.id.navigation).performClick();
        }
    }

    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            () -> {
//                    mMap.setMinZoomPreference(15);
                return false;
            };

    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {

                    /*mMap.setMinZoomPreference(12);

                    CircleOptions circleOptions = new CircleOptions();
                    circleOptions.center(new LatLng(location.getLatitude(),
                            location.getLongitude()));

                    circleOptions.radius(200);
                    circleOptions.fillColor(Color.RED);
                    circleOptions.strokeWidth(6);

                    mMap.addCircle(circleOptions);*/
                    mMap.clear();
                    curLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(curLatLng).draggable(true));
                }
            };

    private void showDefaultLocation() {
        Toast.makeText(this, "Location permission not granted, " +
                        "showing default location",
                Toast.LENGTH_SHORT).show();
        /*curLatLng = new LatLng(22.5726, 88.3639);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(curLatLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(curLatLng, 10F));*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(result1 -> {
            final Status status = result1.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    Log.i(TAG, "All location settings are satisfied.");
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                    try {
                        // Show the dialog by calling startResolutionForResult(), and check the result
                        // in onActivityResult().
                        status.startResolutionForResult(MapsActivity.this, 567);
                    } catch (IntentSender.SendIntentException e) {
                        Log.i(TAG, "PendingIntent unable to execute request.");
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                    break;
            }
        });
    }

    void showEditAddressDialog(Double lat, Double lon, Address a) {
        final Dialog dialog = new Dialog(MapsActivity.this);
        dialog.setContentView(R.layout.dialog_update_address);
        dialog.setTitle("Please Fill these additional details...");

        EditText landmark;
        EditText pincode;
        EditText plotno;
        EditText state;
        EditText street;
        EditText city;

        plotno = dialog.findViewById(R.id.input_plot);
        street = dialog.findViewById(R.id.input_street);
        landmark = dialog.findViewById(R.id.input_landmark);
        state = dialog.findViewById(R.id.input_state);
        city = dialog.findViewById(R.id.input_city);
        pincode = dialog.findViewById(R.id.input_pincode);

        pincode.setText(a.getPostalCode());
        city.setText(a.getLocality());
        plotno.setText(a.getFeatureName());
        landmark.setText(a.getSubAdminArea());
        street.setText(a.getSubLocality());
        state.setText(a.getAdminArea());

        Button btn_update = dialog.findViewById(R.id.btn_update);

        btn_update.setOnClickListener(v -> {

            String splotno = plotno.getText().toString();
            String sstreet = street.getText().toString();
            String slandmark = landmark.getText().toString();
            String sstate = state.getText().toString();
            String scity = city.getText().toString();
            String spincode = pincode.getText().toString();

            if (splotno.matches("") || sstreet.matches("") || slandmark.matches("") || sstate.matches("") || scity.matches("") || spincode.matches("")) {
                Toast.makeText(getApplicationContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
            } else if (spincode.length() < 6) {
                Toast.makeText(getApplicationContext(), "Please fill 6-digit Pin", Toast.LENGTH_SHORT).show();
            } else {
                register("", splotno, sstreet, slandmark, sstate, scity, spincode, lat.toString(), lon.toString());
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    private void register(String... params) {

        final ProgressDialog loading;
        loading = new ProgressDialog(MapsActivity.this);
        loading.setMessage(Html.fromHtml("<b><h2>Please Wait...."));
        loading.show();
        loading.setCancelable(false);
        loading.setCanceledOnTouchOutside(false);

        HashMap<String, String> data = new HashMap<>();
        data.put("plot", params[1]);
        data.put("street", params[2]);
        data.put("landmark", params[3]);
        data.put("state", params[4]);
        data.put("city", params[5]);
        data.put("pin", params[6]);
        data.put("lat", params[7]);
        data.put("lon", params[8]);

        FirebaseFirestore.getInstance().collection("Users")
                .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .set(data, SetOptions.merge())
                .addOnCompleteListener(task -> {
                    loading.dismiss();
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(), "sucess", Toast.LENGTH_SHORT).show();
                        finish();
                    } else
                        Toast.makeText(getApplicationContext(), task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.i(TAG, "onActivityResult() called with: " + "requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        if (requestCode == 567) {
            if (resultCode == Activity.RESULT_OK) {
                new Handler().postDelayed(this::enableMyLocationIfPermitted, 5000);
            } else {
                Toast.makeText(getApplicationContext(), "pls turn on location to fetch location automatically", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
