package com.prakritifresh.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View.GONE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.functions.FirebaseFunctions
import com.google.gson.Gson
import com.mrittica.constants.OrderTypes
import com.paytm.pgsdk.PaytmOrder
import com.paytm.pgsdk.PaytmPGService
import com.paytm.pgsdk.PaytmPaymentTransactionCallback
import com.prakritifresh.R
import com.prakritifresh.paytm.CredentialsType
import com.prakritifresh.paytm.PaytmFactory
import com.prakritifresh.utils.Constants
import com.prakritifresh.viewModels.SubscriptionViewModel
import kotlinx.android.synthetic.main.activity_payment.*
import java.util.*

class PaymentActivity : AppCompatActivity(), PaytmPaymentTransactionCallback {

    private lateinit var functions: FirebaseFunctions
    private val currentUser = FirebaseAuth.getInstance().currentUser!!
    private var orderId: String = "";
    private var type: String = "";
    private var status = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        paytmPay()
        btn_pay_now.performClick()
    }

    private fun paytmPay() {

        functions = FirebaseFunctions.getInstance()
        orderId = intent.extras!!.getString("orderId", "")
        type = intent.extras!!.getString("type", "orders")
        val orderType = intent.extras!!.getString("type", "orders")
        var amount: String = intent.extras!!.getString("amount", "")
        val shopID: String = intent.extras!!.getString("shopID", "")

        if (amount.toFloat() < 99.99  && orderType == OrderTypes.ONETIME.name)
            amount = amount.toFloat().plus(20.00).toString()

        btn_pay_now.setOnClickListener {
            /*val paytmPGService = PaytmPGService.getStagingService()
            val cred = PaytmFactory.getCredentials(CredentialsType.STAGING)*/
            val paytmPGService = PaytmPGService.getProductionService()
            val cred = PaytmFactory.getCredentials(CredentialsType.PROD)
            val paramMap = HashMap<String, String>()
            paramMap["MID"] = cred.MID
            paramMap["ORDER_ID"] = orderId
            paramMap["CUST_ID"] = currentUser.uid
            paramMap["MOBILE_NO"] = currentUser.phoneNumber!!.substringAfter("+91")
//            if (currentUser.email.isNullOrBlank()) paramMap["EMAIL"] = currentUser.email
            paramMap["CHANNEL_ID"] = cred.CHANNEL_ID
            paramMap["TXN_AMOUNT"] = amount
            paramMap["WEBSITE"] = cred.WEBSITE
            paramMap["INDUSTRY_TYPE_ID"] = cred.INDUSTRY_TYPE_ID
            paramMap["CALLBACK_URL"] = cred.CALLBACK_URL + orderId

            val url = cred.generateChecksumUrl
            val queue = Volley.newRequestQueue(this)
            val stringRequest = object : StringRequest(Method.POST, url,
                    Response.Listener { response ->
                        Log.i(Constants.TAG, response)
                        val cech = Gson().fromJson(response.toString(), Map::class.java)
                        paramMap["CHECKSUMHASH"] = cech["CHECKSUMHASH"].toString()
                        val paytmOrder = PaytmOrder(paramMap)
                        paytmPGService.initialize(paytmOrder, null)
                        paytmPGService.startPaymentTransaction(this, true, true, this)
                    },
                    Response.ErrorListener { error ->
                        Log.e(Constants.TAG, error.toString())
                        Toast.makeText(applicationContext, error.toString(), Toast.LENGTH_SHORT).show()
                    }) {
                override fun getParams(): MutableMap<String, String> {
                    return paramMap
                }
            }
            queue.add(stringRequest)
        }
    }

    override fun someUIErrorOccurred(inErrorMessage: String?) {
        Log.d(Constants.TAG, "UI Error Occur.")
        Toast.makeText(applicationContext, " UI Error Occur. ", Toast.LENGTH_LONG).show()
    }

    override fun onTransactionResponse(inResponse: Bundle) {
        Log.d(Constants.TAG, "Payment Transaction : $inResponse")
//        Toast.makeText(applicationContext, "Payment Transaction response $inResponse", Toast.LENGTH_LONG).show()
        if (inResponse["STATUS"].toString() == "TXN_SUCCESS") {
            status = true
            btn_pay_now.visibility = GONE
//            pl.visibility = GONE
//            tv_paid.visibility = View.VISIBLE
            FirebaseFirestore.getInstance().collection(type)
                    .document(orderId)
                    .update("payementMode", "PayTm")
            SubscriptionViewModel.setIsCached()
            setResult(235, Intent().putExtra("status", status))
            finish()
        } else Toast.makeText(applicationContext, inResponse["STATUS"].toString(), Toast.LENGTH_LONG).show()
    }

    override fun networkNotAvailable() {
        Log.d(Constants.TAG, "UI Error Occur.")
        Toast.makeText(applicationContext, " Network Error Occurred. ", Toast.LENGTH_LONG).show()
    }

    override fun clientAuthenticationFailed(inErrorMessage: String) {
        Log.d(Constants.TAG, "UI Error Occur.")
        Toast.makeText(applicationContext, " Sever side Error $inErrorMessage", Toast.LENGTH_LONG).show()
    }

    override fun onErrorLoadingWebPage(iniErrorCode: Int,
                                       inErrorMessage: String?, inFailingUrl: String?) {
    }

    override fun onBackPressedCancelTransaction() { // TODO Auto-generated method stub
    }

    override fun onTransactionCancel(inErrorMessage: String, inResponse: Bundle?) {
        Log.d(Constants.TAG, "Payment Transaction Failed $inErrorMessage")
        Toast.makeText(baseContext, "Payment Transaction Failed ", Toast.LENGTH_LONG).show()
    }

    override fun onBackPressed() {
        setResult(235, Intent().putExtra("status", status))
        finish()
        //super.onBackPressed()
    }
}
