package com.prakritifresh.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.firebase.ui.storage.images.FirebaseImageLoader
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.storage.FirebaseStorage
import com.mrittica.utils.convertToString
import com.prakritifresh.GlideApp
import com.prakritifresh.MyApp
import com.prakritifresh.R
import com.prakritifresh.utils.Constants
import kotlinx.android.synthetic.main.activity_story.*


class StoryActivity : AppCompatActivity() {

    private var farmer: String = ""
    private var item: String = ""
    private var region: String = ""
    private var packedTime: String = ""
    private var trackId: String = ""
    private var packsId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story)

        tv_story.text = "This is $item grown by $farmer. He is a resident of $region. The item is packed on $packedTime";

        val hashMap = intent.getSerializableExtra("ids") as HashMap<String, String>

        trackId = hashMap["trackid"].toString()
        packsId = hashMap["packid"].toString()
    }

    override fun onResume() {
        super.onResume()


        MyApp.db.collection("tracks").document(trackId)
                .get()
                .addOnSuccessListener { documentSnapshot ->
                    farmer = documentSnapshot.get("farmerName").toString()
                    region = documentSnapshot.get("clusterID").toString().substring(0, 6)

                    MyApp.db.collection("tracks").document(trackId).collection("packs").document(packsId)
                            .get()
                            .addOnSuccessListener { packs_doc ->

                                packedTime = packs_doc.getTimestamp("packedtime")!!.toDate().time.convertToString(Constants.ordersDateFormat)
                                item = packs_doc.get("itemID").toString()

                                MyApp.db.collection("central_data")
                                        .document("regions")
                                        .get()
                                        .addOnSuccessListener { region_doc: DocumentSnapshot ->
                                            val map: Map<String, String> = region_doc.data as Map<String, String>

                                            region = map[region].toString()


                                            val storageReference = FirebaseStorage.getInstance().reference.child("itemImages/$item.jpg")

                                            GlideApp.with(applicationContext)
                                                    .load(storageReference)
                                                    .error(R.mipmap.prakritifresh_logo)
                                                    .into(iv_item)

                                            MyApp.db.collection("itemlist")
                                                    .get()
                                                    .addOnSuccessListener { c ->
                                                        val map = c.documents
                                                        val my_item_list: MutableMap<String, String> = java.util.HashMap()
                                                        for (doc in map) {
                                                            for ((item_name, subs_map) in doc.data!!) {

                                                                for ((subcat, item_id) in subs_map as Map<String, String>) {
                                                                    if (subcat != "main") {
                                                                        my_item_list[item_id.toString()] = item_name + " , " + subcat
                                                                    } else {
                                                                        my_item_list[item_id.toString()] = item_name
                                                                    }

                                                                }

                                                                tv_story.text = "This is ${my_item_list[item]} grown by $farmer. He is a resident of $region, where he has been practicing organic farming with us. The item is packed on $packedTime. \n\n Please enjoy the product. Thanks for being part of this amazing journey to organic. \n\n\n -Praktitifresh, Hoping to Serve you best.";

                                                            }
                                                        }
                                                    }
                                        }

                            }
                }
    }

}
