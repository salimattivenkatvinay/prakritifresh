package com.prakritifresh.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.prakritifresh.R
import kotlinx.android.synthetic.main.activity_gift.*

class GiftActivity : AppCompatActivity() {

    private val db: FirebaseFirestore = FirebaseFirestore.getInstance();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gift)

        btn_claim.setOnClickListener {
            it.visibility = View.GONE
            pb_gift.visibility = View.VISIBLE
            getGifts()
        }

        ib_close.setOnClickListener { onBackPressed() }
    }

    private fun getGifts() {
        db.collection("gifts")
                .get()
                .addOnSuccessListener { qS ->
                    if (qS.isEmpty.not()) {
                        for (doc in qS.documents) {
                            val m = doc.data
                            m?.set("id", doc.id)
                        }
                    }
                }
    }
}
