package com.prakritifresh.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest.Builder
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.prakritifresh.BuildConfig
import com.prakritifresh.R
import com.prakritifresh.utils.AppVersionStatuses
import com.prakritifresh.utils.Constants
import kotlinx.android.synthetic.main.activity_splash.*


class SplashScreenActivity : AppCompatActivity() {

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)

        val attrib = window.attributes
        attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        checkUpdate()

        btn_update_check_retry.setOnClickListener {
            checkUpdate()
        }
        btn_update_app.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.prakritifresh.user1")))
        }
    }

    private fun checkUpdate() {

        pb_check_app_update.visibility = View.VISIBLE
        btn_update_app.visibility = View.GONE
        btn_update_check_retry.visibility = View.GONE


        FirebaseFirestore.getInstance().collection("userAppVersions")
                .document("app")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {

                        val m: AppVersionStatuses = task.result?.toObject(AppVersionStatuses::class.java)!!

                        when (BuildConfig.VERSION_CODE) {
                            in 0..m.disabled -> {
                                Toast.makeText(applicationContext, "please update app", Toast.LENGTH_SHORT).show()
                                pb_check_app_update.visibility = View.GONE
                                btn_update_app.visibility = View.VISIBLE
                            }
                            in (m.disabled + 1)..m.deprecated -> {
                                Toast.makeText(applicationContext, "please update app", Toast.LENGTH_SHORT).show()
                                pb_check_app_update.visibility = View.GONE
                                btn_update_app.visibility = View.VISIBLE
                            }
                            in m.deprecated + 1 until m.current -> {
                                Toast.makeText(applicationContext, "New Version of App available", Toast.LENGTH_SHORT).show()
                                openApp()
                            }
                            else -> {
                                openApp()
                            }
                        }
                    } else {
                        Log.e(Constants.TAG, task.exception.toString())
                        Toast.makeText(applicationContext, "Network not available. Try after some time", Toast.LENGTH_SHORT).show()
                        pb_check_app_update.visibility = View.GONE
                        btn_update_check_retry.visibility = View.VISIBLE
                    }
                }
    }

    private fun openApp() {
//        return
        startActivity(Intent(this@SplashScreenActivity, HomeActivity::class.java))
        finish()
    }

    private fun updateUserName() {
        FirebaseFirestore.getInstance().collection("Users")
                .document(FirebaseAuth.getInstance().uid!!)
                .get()
                .addOnSuccessListener { documentSnapshot: DocumentSnapshot ->
                    if (documentSnapshot.exists()) {
                        FirebaseAuth.getInstance().currentUser!!.updateProfile(Builder()
                                .setDisplayName(documentSnapshot.get("uname")?.toString())/*.setPhotoUri(Uri.parse(""))*/
                                .build())
                    }
                }
    }
}