package com.prakritifresh.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.florent37.inlineactivityresult.kotlin.startForResult
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.prakritifresh.R
import com.mrittica.Order
import com.prakritifresh.utils.Constants.TAG
import kotlinx.android.synthetic.main.activity_thankyou.*
import java.util.*

class ThankYouActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_thankyou)
        homepage.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
            finishAffinity()
        }
        btn_pay_now.setOnClickListener {
            startForResult(Intent(this, PaymentActivity::class.java)
                    .putExtras(intent)) { result ->
                if (result.data?.getBooleanExtra("status", false)!!) {
                    btn_pay_now.visibility = View.GONE
                    pl.visibility = View.GONE
                    tv_paid.visibility = View.VISIBLE
                }
            }.onFailed { result -> Toast.makeText(applicationContext, result.toString(), Toast.LENGTH_SHORT).show() }
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this, HomeActivity::class.java))
        finishAffinity()
    }

    private fun checkForGift() {
        val thisMonth = Date()
        thisMonth.date = 1
        thisMonth.hours = 0
        thisMonth.minutes = 0
        thisMonth.seconds = 0
        Log.i(TAG, "checkForGift: $thisMonth")
        FirebaseFirestore.getInstance().collection("orders")
                .whereEqualTo("user_id", FirebaseAuth.getInstance().uid)
                .whereGreaterThan("orderedTime", thisMonth.time)
                .get()
                .addOnCompleteListener { task: Task<QuerySnapshot> ->
                    if (task.isSuccessful) {
                        if (task.result!!.isEmpty) {
                            Log.i(TAG, "no orders in this month")
                        } else {
                            var totalThisMonth = 0f
                            for (documentSnapshot in task.result!!) {
                                val m = documentSnapshot.toObject(Order::class.java)
                                m.order_id = documentSnapshot.id
                                Log.i(TAG, "Odered Date: " + Date(m.orderedTime))
                                totalThisMonth += m.amount
                            }
                            Log.i(TAG, "checkForGift: total this month: $totalThisMonth")
                            if (totalThisMonth > 1199f) {
                                startActivity(Intent(this@ThankYouActivity, GiftActivity::class.java))
                            }
                        }
                    }
                }
    }

}