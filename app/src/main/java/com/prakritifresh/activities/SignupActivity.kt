package com.prakritifresh.activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.prakritifresh.R
import kotlinx.android.synthetic.main.activity_signup.*
import java.util.*

class SignupActivity : AppCompatActivity() {

    private lateinit var emaili: TextInputLayout
    private lateinit var edit_email: EditText
    private lateinit var phone: EditText
    private var et_P: EditText? = null
    private var et_uname: EditText? = null
    private var progressDialog: ProgressDialog? = null
    private var radioSexGroup: RadioGroup? = null
    private lateinit var radioSexButton: RadioButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        emaili = findViewById(R.id.emaili)
        edit_email = findViewById(R.id.input_email)
        val mobi = findViewById<TextInputLayout>(R.id.mobi)
        phone = findViewById(R.id.input_phone)
        et_P = findViewById(R.id.et_p)
        phone.setText(FirebaseAuth.getInstance().currentUser!!.phoneNumber)
        phone.isEnabled = false
        et_uname = findViewById(R.id.input_fname)
        tv_temp.text = Html.fromHtml("We at <B>Prakritifresh</B>, bring chemical<br> free agro products at your finger tip.")
        val buttonRegister = findViewById<Button>(R.id.btn_signup)
        radioSexGroup = findViewById(R.id.radioGroup)
        emaili.editText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (Patterns.EMAIL_ADDRESS.matcher(s).matches() || s.toString().trim { it <= ' ' }.isEmpty()) {
                    emaili.error = null
                    return
                }
                emaili.isErrorEnabled = true
                emaili.error = "Please Enter a Valid Email"
            }

            override fun afterTextChanged(s: Editable) {
                if (Patterns.EMAIL_ADDRESS.matcher(s).matches() || s.toString().trim { it <= ' ' }.isEmpty()) {
                    emaili.error = null
                    emaili.isErrorEnabled = false
                    return
                }
                emaili.isErrorEnabled = true
                emaili.error = "Please Enter a Valid Email"
            }
        })
        buttonRegister.setOnClickListener { v: View? -> registerUser() }
        tv_terms.setOnClickListener { v: View? -> startActivity(Intent(this, TextActivity::class.java).putExtra("path", "Mrittica_Terms_and_Conditions.pdf")) }
        tv_privacy_policy.setOnClickListener { v: View? -> startActivity(Intent(this, TextActivity::class.java).putExtra("path", "Mrittica_Privacy_Policy.pdf")) }
        progressDialog = ProgressDialog(this@SignupActivity)
        progressDialog!!.setMessage("Submitting details ...")
        progressDialog!!.setCanceledOnTouchOutside(false)
        data
    }

    private fun registerUser() {
        val uname = et_uname!!.text.toString()
        val sedit_email = edit_email.text.toString()
        val sphone = phone.text.toString()
        val p = et_P!!.text.toString()
        val selectedId = radioSexGroup!!.checkedRadioButtonId
        radioSexButton = findViewById(selectedId)
        val gender = radioSexButton.text.toString()
        signUp(sedit_email, uname, sphone, p, gender)
    }

    fun signUp(email: String, uname: String, phno: String, p: String, gender: String) {
        progressDialog!!.show()
        val data: MutableMap<String, String> = HashMap()
        data["uname"] = uname
        data["phno"] = phno
        data["mail"] = email
        data["profession"] = p
        data["gender"] = gender
        FirebaseFirestore.getInstance().collection("Users")
                .document(FirebaseAuth.getInstance().currentUser!!.uid)
                .set(data, SetOptions.merge())
                .addOnCompleteListener { task: Task<Void?> ->
                    progressDialog!!.hide()
                    if (task.isSuccessful) {
                        FirebaseAuth.getInstance().currentUser!!.updateProfile(UserProfileChangeRequest.Builder()
                                .setDisplayName(uname)
                                .build()).addOnCanceledListener {
                            Toast.makeText(applicationContext, uname, Toast.LENGTH_SHORT).show()
                        }
                        Toast.makeText(applicationContext, "sucess", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this, MapsActivity::class.java))
                        finish()
                    } else Toast.makeText(applicationContext, task.exception!!.localizedMessage, Toast.LENGTH_SHORT).show()
                }
    }

    val data: Unit
        get() {
            progressDialog!!.show()
            FirebaseFirestore.getInstance().collection("Users")
                    .document(FirebaseAuth.getInstance().currentUser!!.uid)
                    .get()
                    .addOnCompleteListener { documentSnapshot: Task<DocumentSnapshot> ->
                        progressDialog!!.hide()
                        if (documentSnapshot.result!!.exists()) {
                            val m: Map<*, *>? = documentSnapshot.result!!.data
                            if (m!!.containsKey("uname")) et_uname!!.setText(m["uname"].toString())
                            if (m.containsKey("mail")) edit_email.setText(m["mail"].toString())
                            if (m.containsKey("profession")) et_P!!.setText(m["profession"].toString())
                            if (m.containsKey("gender")) {
                                if (m["gender"].toString().toLowerCase() == "male") {
                                    radioSexButton = findViewById(R.id.radioButton)
                                    radioSexButton.isChecked = true
                                } else {
                                    radioSexButton = findViewById(R.id.radioButton2)
                                    radioSexButton.isChecked = true
                                }
                            }
                        }
                    }
        }
}