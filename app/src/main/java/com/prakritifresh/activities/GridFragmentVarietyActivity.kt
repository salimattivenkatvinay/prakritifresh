package com.prakritifresh.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.GridView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.mrittica.ShopItem
import com.mrittica.utils.toTitleCase
import com.prakritifresh.R
import com.prakritifresh.adapters.GridAdapterVariety
import com.prakritifresh.listners.UpdateListener
import com.prakritifresh.utils.SharedPrefsUtil

class GridFragmentVarietyActivity : AppCompatActivity(), UpdateListener {
    var aList: ArrayList<ShopItem> = ArrayList()
    var adapter: GridAdapterVariety? = null
    var grid: GridView? = null
    var textView: TextView? = null
    private var tv_cart: TextView? = null
    var loading: ProgressBar? = null
    var toolbar: Toolbar? = null
    var i = 0
    var db: FirebaseFirestore? = null
    var type = ""

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.primary_grid_layout_variety)
        loading = findViewById(R.id.pb)
        textView = findViewById(R.id.tvar)
        grid = findViewById(R.id.gridview1)
        tv_cart = findViewById(R.id.textOne)
        type = intent.extras!!.getString("name", "banana")
        toolbar = findViewById(R.id.toolbar)
        (toolbar?.findViewById<View>(R.id.type) as TextView).text = type.toTitleCase()
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        findViewById<View>(R.id.cart_item).setOnClickListener { startActivity(Intent(this, CartActivity::class.java)) }
        getJSON(type)
    }

    override fun onResume() {
        super.onResume()
        updatecart()
        if (aList.size > 0) {
            adapter!!.notifyDataSetChanged()
        }
    }

    private fun getJSON(type: String) {
        db = FirebaseFirestore.getInstance()
 /*       db!!.collection("pincodes")
                .document(SharedPrefsUtil.getInstance(this).pincode)
                .get()
                .addOnSuccessListener { documentSnapshot: DocumentSnapshot ->
                    val temp: Map<*, *>? = documentSnapshot.data
                    for (key in temp!!.keys) {*/
                        db!!.collection("shopitems")
                                .whereEqualTo("pincode", /*key.toString()*/SharedPrefsUtil.getInstance(this).pincode)
                                .whereEqualTo("name",type /*intent.extras!!.getString("name")*/)
                                .get()
                                .addOnSuccessListener { queryDocumentSnapshots: QuerySnapshot ->
                                    i++
                                    if (!queryDocumentSnapshots.isEmpty) {
                                        for (documentSnapshot1 in queryDocumentSnapshots) {
                                            val shopItem = documentSnapshot1.toObject(ShopItem::class.java)
                                            shopItem.id = documentSnapshot1.id
                                            if (shopItem.available == "no" || shopItem.unitqty.toFloat() > shopItem.available_quantity)
                                                aList.add(shopItem) else aList.add(0, shopItem)
                                        }
                                    }
//                                    if (i == temp.size) {
                                        loading!!.visibility = View.GONE
                                        showJSON()
//                                    }
                                }
                                .addOnFailureListener { e: Exception? -> loading!!.visibility = View.GONE }
//                    }
//                }
//                .addOnFailureListener { loading!!.visibility = View.GONE }
    }

    private fun showJSON() {
        adapter = GridAdapterVariety(this, aList, this)
        grid!!.adapter = adapter
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Animatoo.animateSlideRight(this) //fire the slide left animation
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun updatecart() {
        tv_cart!!.text = SharedPrefsUtil.getInstance(this).cartSize.toString()
    }

    override fun cartUpdate() {
        updatecart()
    }
}