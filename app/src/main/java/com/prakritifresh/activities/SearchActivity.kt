package com.prakritifresh.activities

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mrittica.ShopItem
import com.prakritifresh.R
import com.prakritifresh.adapters.GridAdapterVariety
import com.prakritifresh.listners.UpdateListener
import com.prakritifresh.utils.SharedPrefsUtil
import com.prakritifresh.viewModels.ShopItemsViewModel
import kotlinx.android.synthetic.main.activity_search.*
import java.util.*


class SearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener, UpdateListener {

    private val sList = ArrayList<ShopItem>()
    internal var adapter: GridAdapterVariety? = null
    private lateinit var tv_cart: TextView

    private val catComparator = Comparator<ShopItem> { a, b -> ((if (a.subcat == "main") "" else a.subcat) + a.name).compareTo((if (b.subcat == "main") "" else b.subcat) + b.name) }
    private var aList: SortedSet<ShopItem> = TreeSet(catComparator)
//    private var bList: SortedSet<ShopItem> = TreeSet(catComparator)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        tv_cart = findViewById(R.id.textOne)

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { _ ->
            startActivity(Intent(applicationContext, CartActivity::class.java))
        }

        getData()
    }

    override fun onResume() {
        super.onResume()
        updatecart()
        if (adapter != null) {
            showJSON()
        }
    }

    private fun getData() {
        val loading = ProgressDialog(this)
        loading.setTitle("fetching items...")
        loading.setCanceledOnTouchOutside(false)
        loading.show()
/*        MyApp.db.collection("pincodes")
                .document(getSharedPreferences(Constants.db, Context.MODE_PRIVATE).getString("pincode", "736122")!!)
                .get()
                .addOnSuccessListener { documentSnapshot ->
                    val temp = documentSnapshot.data as Map<String, *>?
                    if (temp == null) {
                        loading.dismiss()
                        return@addOnSuccessListener
                    }
                    MyApp.db.collection("shopitems")
                            .whereIn("pincode", temp.keys.toMutableList())
                            .get()
                            .addOnSuccessListener { queryDocumentSnapshots ->
                                loading.dismiss()
                                if (!queryDocumentSnapshots.isEmpty) {
                                    for (documentSnapshot1 in queryDocumentSnapshots) {
                                        val name = documentSnapshot1.toObject(ShopItem::class.java)
                                        name.id = documentSnapshot1.id
                                        if (name.available == "no" || name.unitqty.toFloat() > name.available_quantity)
//                                                bList.add(name)
                                        else
                                            aList.add(name)
                                    }
                                }
                                sList.addAll(aList)
//                                        sList.addAll(bList)
                                showJSON()

                            }
                            .addOnFailureListener { e -> loading.dismiss(); }

                }
                .addOnFailureListener { e -> loading.dismiss(); }*/

        ShopItemsViewModel.getAllShopItems(SharedPrefsUtil.getInstance(this).pincode, false) {
            loading.dismiss()
            sList.addAll(it)
            if (sList.size > 0)
                showJSON()
        }
    }

    private fun showJSON() {
        adapter = GridAdapterVariety(this, sList, this)
        gridview1?.adapter = adapter
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val item = menu.findItem(R.id.action_search)
        val searchView = MenuItemCompat.getActionView(item) as SearchView
        searchView.setOnQueryTextListener(this)
        searchView.isIconified = false
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false; }

    override fun onQueryTextChange(newText: String?): Boolean {
        adapter?.setFilter(filter(sList, newText?.toLowerCase()))
        return true
    }

    private fun filter(dataList: ArrayList<ShopItem>, newText: String?): ArrayList<ShopItem> {
        if (newText == null) return dataList
        val aList = ArrayList<ShopItem>()
        val bList = ArrayList<ShopItem>()

        for (sh in dataList) {
            if (sh.subcat == "main") {
                if (sh.name.contains(newText, true))
                    aList.add(sh)
            } else {
                if (sh.subcat.contains(newText, true))
                    aList.add(sh)
                else if (sh.name.contains(newText, true))
                    bList.add(sh)
            }
        }
        aList.addAll(bList)
        return aList
    }


    private fun updatecart() {
        tv_cart.text = SharedPrefsUtil.getInstance(this).cartSize.toString()
    }

    override fun cartUpdate() {
        //Log.i(this.localClassName, "called")
        updatecart()
    }
}