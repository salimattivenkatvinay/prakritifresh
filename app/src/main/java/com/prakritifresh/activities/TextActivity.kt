package com.prakritifresh.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.prakritifresh.R
import kotlinx.android.synthetic.main.activity_text.*

class TextActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text)

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Details"

        pdfView.fromAsset("html/${intent.extras!!.getString("path")}").load()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
