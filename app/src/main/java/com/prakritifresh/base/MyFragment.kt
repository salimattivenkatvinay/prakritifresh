package com.prakritifresh.base

import androidx.fragment.app.Fragment
import com.mrittica.constants.OrderStatus
import com.prakritifresh.utils.Constants

public open class MyFragment : Fragment(), Constants, OrderStatus