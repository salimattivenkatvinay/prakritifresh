package com.prakritifresh.base

import androidx.appcompat.app.AppCompatActivity
import com.mrittica.constants.OrderStatus
import com.prakritifresh.utils.Constants

open class MyActivity : AppCompatActivity(), Constants, OrderStatus