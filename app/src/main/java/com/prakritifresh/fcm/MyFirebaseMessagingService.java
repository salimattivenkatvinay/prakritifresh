package com.prakritifresh.fcm;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.prakritifresh.activities.SplashScreenActivity;
import com.prakritifresh.utils.Constants;


import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class MyFirebaseMessagingService extends FirebaseMessagingService implements Constants {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);
        // Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
        Log.i(TAG, "onNewToken: " + s);
        sendRegistrationToServer(s);
    }

    public void sendRegistrationToServer(String refreshedToken) {
        Log.i(TAG, "sendRegistrationToServer: " + refreshedToken);
        if (FirebaseAuth.getInstance().getUid() == null) return;
        db.collection("Users").document(FirebaseAuth.getInstance().getUid())
                .set(new HashMap<String, String>() {{
                    put("token", refreshedToken);
                }}, SetOptions.merge());
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

        }

        startActivity(new Intent(this, SplashScreenActivity.class));
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }
}
