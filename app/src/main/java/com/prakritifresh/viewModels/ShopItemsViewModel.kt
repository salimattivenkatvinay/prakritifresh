package com.prakritifresh.viewModels

import android.util.Log
import com.mrittica.ShopItem
import com.prakritifresh.MyApp
import com.prakritifresh.utils.Constants.TAG
import java.util.concurrent.CopyOnWriteArrayList

public object ShopItemsViewModel {

    private var lastCached = 0L
    private val shopItems: CopyOnWriteArrayList<ShopItem> = CopyOnWriteArrayList()
    private val shopItemsOutOfStock: CopyOnWriteArrayList<ShopItem> = CopyOnWriteArrayList()

    public fun getAllShopItems(
        pincode: String = "736122",
        includeOutOfStock: Boolean = true,
        some: (List<ShopItem>) -> Unit
    ) {
        if (System.currentTimeMillis() - lastCached < 10 * 60 * 1000)
            some(getCachedAllShopItems(includeOutOfStock))
        else
         MyApp.db.collection("pincodes")
                 .document(pincode)
                 .get()
                 .addOnSuccessListener { documentSnapshot ->
                     Log.i(TAG, "pincodes: ${documentSnapshot.data}")
                     (documentSnapshot.data as Map<String, *>?)?.let {
            MyApp.db.collection("shopitems")
                .whereIn("pincode", it.keys.toMutableList()/*listOf(pincode)*/)
                .get()
                .addOnSuccessListener { queryDocumentSnapshots ->
                    Log.i(TAG, "getAllShopItems: ${queryDocumentSnapshots.map { it.data }}")
                    lastCached = System.currentTimeMillis()
                    if (!queryDocumentSnapshots.isEmpty) {
                        for (documentSnapshot1 in queryDocumentSnapshots) {
                            val name = documentSnapshot1.toObject(ShopItem::class.java)
                            name.id = documentSnapshot1.id
                            if (name.available == "no" || name.unitqty.toFloat() > name.available_quantity)
                                shopItemsOutOfStock.add(name)
                            else
                                shopItems.add(name)
                        }
                    }
                    some(getCachedAllShopItems(includeOutOfStock))
                }
                .addOnFailureListener {
                    some(getCachedAllShopItems())
                }
                        } ?: some(getCachedAllShopItems())
        }
        .addOnFailureListener {
            getCachedAllShopItems()
        }
    }

    fun getCachedAllShopItems(includeOutOfStock: Boolean = false): CopyOnWriteArrayList<ShopItem> {
        val temp: CopyOnWriteArrayList<ShopItem> = CopyOnWriteArrayList()
        temp.addAll(shopItems)
        if (includeOutOfStock)
            temp.addAll(shopItemsOutOfStock)
        return temp
    }

    public fun getFilteredShopItems(
        pincode: String = "736122",
        includeOutOfStock: Boolean = true,
        filter: List<String>,
        some: (List<ShopItem>) -> Unit
    ) {
        getAllShopItems(pincode, includeOutOfStock) {
            some(it.filter { filter.contains(it.id) })
        }
    }

    public fun getFilterdVarietyShopItems(
        pincode: String = "736122",
        includeOutOfStock: Boolean = true,
        filter: String?,
        some: (List<ShopItem>) -> Unit
    ) {
        getAllShopItems(pincode, includeOutOfStock) {
            some(it.filter { it.category == filter })
        }
    }

    public fun resetCache() {
        this.lastCached = 0L
        shopItems.clear()
        shopItemsOutOfStock.clear()
    }
}