package com.prakritifresh.viewModels

import com.google.firebase.auth.FirebaseAuth
import com.prakritifresh.MyApp
import com.prakritifresh.pojos.SubscriptionF
import com.prakritifresh.utils.Constants
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList

object SubscriptionViewModel {

    private var isCached = false
    private val subscriptions: CopyOnWriteArrayList<SubscriptionF> = CopyOnWriteArrayList()

    fun getSubscriptions(some: (List<SubscriptionF>) -> Unit) {
        if (isCached)
            some(subscriptions)
        else
            MyApp.db.collection(Constants.subscriptions)
                    .whereEqualTo("userId", FirebaseAuth.getInstance().uid)
                    .get()
                    .addOnSuccessListener {
                        isCached = true
                        it.documents.forEach { doc ->
                            val temp = doc.toObject(SubscriptionF::class.java)!!
                            temp.id = doc.id
                            subscriptions.add(temp)
                        }
                        some(subscriptions)
                    }.addOnFailureListener {
                        some(subscriptions)
                    }
    }

    fun isPlusUser(some: (String?) -> Unit) {
        getSubscriptions { list ->
            some(list.firstOrNull { it.endDate!! > Date()}?.status)
        }
    }

    fun getActiveSubscriptionId(some: (String?) -> Unit) {
        getSubscriptions { list ->
            some(list.firstOrNull { it.endDate!! > Date() }?.id)
        }
    }

    fun getSubscriptionDetails(subscriptionId: String, some: (SubscriptionF?) -> Unit) {
        getSubscriptions { list ->
            some(list.find { it.id == subscriptionId })
        }
    }


    fun setIsCached(boolean: Boolean = false) {
        this.isCached = boolean
    }

}