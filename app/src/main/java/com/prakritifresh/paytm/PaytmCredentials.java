package com.prakritifresh.paytm;

public class PaytmCredentials {

    public String MID, CHANNEL_ID, WEBSITE, INDUSTRY_TYPE_ID, CALLBACK_URL, generateChecksumUrl;

    PaytmCredentials(String MID, String CHANNEL_ID, String WEBSITE, String INDUSTRY_TYPE_ID, String CALLBACK_URL, String generateChecksumUrl) {
        this.MID = MID;
        this.CHANNEL_ID = CHANNEL_ID;
        this.WEBSITE = WEBSITE;
        this.INDUSTRY_TYPE_ID = INDUSTRY_TYPE_ID;
        this.CALLBACK_URL = CALLBACK_URL;
        this.generateChecksumUrl = generateChecksumUrl;
    }
}
