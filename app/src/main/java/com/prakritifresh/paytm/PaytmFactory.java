package com.prakritifresh.paytm;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class PaytmFactory {

    public static PaytmCredentials getCredentials(CredentialsType credentialsType) {

        switch (credentialsType) {
            case PROD:
                return new PaytmCredentials("WGondE36311551816325", "WAP", "DEFAULT", "Retail", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=", FirebaseRemoteConfig.getInstance().getString("paytm_server_url"));
            case STAGING:
            default:
                return new PaytmCredentials("KEZSpO44768845086091", "WAP", "WEBSTAGING", "Retail", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=", FirebaseRemoteConfig.getInstance().getString("paytm_server_url"));
        }
    }
}
